@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Data Persyaratan
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>ID Daftar</th>
                <th>Jurusan</th>
                <th>Photo</th>
                <th>SKL</th>
                <th>Rapot</th>
                <th>KK</th>
                <th>Telp</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
              @foreach($siswa as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->id_daftar }}</td>
                <td>{{ $item->jurusan_1 }}</td>
                <td>
                  @php ($i = 0)
                  @foreach($item->syarat as $data)
                  @if($data->ket=="pas-photo")
                  @php
                  $i = $i+1
                  @endphp
                  @endif
                  @endforeach
                  @if($i >0)
                  <span class="badge bg-green">Sudah</span>
                  @else
                  <span class="badge bg-red">Belum</span>
                  @endif
                </td>
                <td>
                  @php ($j = 0)
                  @foreach($item->syarat as $data)
                  @if($data->ket=="skl")
                  @php
                  $j = $j+1
                  @endphp
                  @endif
                  @endforeach
                  @if($j >0)
                  <span class="badge bg-green">Sudah</span>
                  @else
                  <span class="badge bg-red">Belum</span>
                  @endif
                </td>
                <td>
                  @php ($k = 0)
                  @foreach($item->syarat as $data)
                  @if($data->ket=="rapot")
                  @php
                  $k = $k+1
                  @endphp
                  @endif
                  @endforeach
                  @if($k >0)
                  <span class="badge bg-green">Sudah</span>
                  @else
                  <span class="badge bg-red">Belum</span>
                  @endif
                </td>
                <td>
                  @php ($l = 0)
                  @foreach($item->syarat as $data)
                  @if($data->ket=="kk")
                  @php
                  $l = $l+1
                  @endphp
                  @endif
                  @endforeach
                  @if($l >0)
                  <span class="badge bg-green">Sudah</span>
                  @else
                  <span class="badge bg-red">Belum</span>
                  @endif
                </td>
                <td>{{ $item->telp }}</td>
                <td><a href="{{ route('admin.persyaratan.show', $item->id) }}" class="btn btn-success">Lihat</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection