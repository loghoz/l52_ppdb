<!DOCTYPE html>
<html>
<head>
	<title>Formulir Pendaftaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        hr.new4 {
            border: 2px solid #000000;
        }
    </style>
</head>
<body onload="window.print()">
	<div class="container">
		<table width="100%">
			<tbody>
				<tr>
					<td>
						<img src="{{asset('itlabil/image/default/logo.png')}}" width="150px">
					</td>
					<td align="center">
						<b><h3>FORMULIR PENDAFTARAN SISWA/I BARU</h3></b>
						<h5><font color="green">SEKOLAH MENENGAH KEJURUAN (SMK)</font><font color="red"> PELITA</font></h5>
						<h4>BISNIS MANAJEMEN, TEKNIK INFORMATIKA DAN KESEHATAN</h4>
						<h6>GEDONGTATAAN KABUPATEN PESAWARAN</h6>
						<h6>TAHUN PELAJARAN 2020/2021</h6>
					</td>
					<td>
						<img src="{{asset('itlabil/image/default/smk_bisa.png')}}" width="200px" height="250px">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="col-md-12" align="center">
							<img src="{{asset('itlabil/image/default/4.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/3.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/5.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/2.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/6.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/7.png')}}" width="50px">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
        <hr class="new4">
		<table width="100%">
			<tbody>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Tempat & Tanggal Lahir</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>NISN</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Kelas</td>
                    <td>: 10 / 11 / 12</td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Asal Sekolah SMP / MTS</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nomor STTB / Ijazah</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nomor SKHU</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nomor Peserta Ujian SMP</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Alamat Siswa/i</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nama Ayah</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Pendidikan Ayah</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Pekerjaan Ayah</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nama Ibu</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Pendidikan Ibu</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Pekerjaan Ibu</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Alamat Orang Tua</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Nomor HP/Telpon</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td colspan="2"><b>Lingkari Jurusan yang dipilih : </b></td>
                </tr>
                <tr>
                    <td>Jurusan 1 </td>
                    <td>
                        <span class="badge badge-info">A</span> Adm. Perkantoran.
                        <span class="badge badge-info">B</span> Akuntansi.
                        <span class="badge badge-info">C</span> Pemasaran.<br>
                        <span class="badge badge-info">D</span>Teknik Kommputer Jaringan.
                        <span class="badge badge-info">E</span> Multimedia.
                        <span class="badge badge-info">F</span> Farmasi
                    </td>
                </tr>
                <tr>
                    <td>Jurusan 2 </td>
                    <td>
                        <span class="badge badge-info">A</span> Adm. Perkantoran.
                        <span class="badge badge-info">B</span> Akuntansi.
                        <span class="badge badge-info">C</span> Pemasaran.<br>
                        <span class="badge badge-info">D</span>Teknik Kommputer Jaringan.
                        <span class="badge badge-info">E</span> Multimedia.
                        <span class="badge badge-info">F</span> Farmasi
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
                <tr>
                    <td>Tanggal Daftar</td>
                    <td>: .....................................................................................................................................................................................</td>
                </tr>
                <tr>
                    <td>Administrasi</td>
                    <td>: Rp. 30.000</td>
                </tr>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
            </tbody>
		</table>
        <table width="100%">
			<tbody>
                <tr>
                    <td colspan="3"><b>Kelengkapan Berkas : </b></td>
                </tr>
                <tr>
                    <td>Foto Copy STTB/Ijazah</td>
                    <td>: 2 Lembar</td>
                    <td>: Ada / Tidak Ada</td>
                </tr>
                <tr>
                    <td>Foto Copy SKHU</td>
                    <td>: 2 Lembar</td>
                    <td>: Ada / Tidak Ada</td>
                </tr>
                <tr>
                    <td>Pas Photo 3x4</td>
                    <td>: 4 Lembar</td>
                    <td>: Ada / Tidak Ada</td>
                </tr>
                <tr>
                    <td>Pas Photo 2x3</td>
                    <td>: 2 Lembar</td>
                    <td>: Ada / Tidak Ada</td>
                </tr>
            </tbody>
		</table>
        <hr>
        <table width="100%">
			<tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="center">Gedongtataan, ...............................................................</td>
                </tr>
                <tr>
                    <td align="center">Petugas Pendaftaran</td>
                    <td></td>
                    <td align="center">Yang Mendaftar</td>
                </tr>
                <tr height="80px">
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="center"><u>(......................................)</u></td>
                    <td></td>
                    <td align="center"><u>(......................................)</u></td>
                </tr>
            </tbody>
		</table>
		<br>
		
        <footer>
            <cite title="Source Title">*Coret yang tidak perlu.</cite>
        </footer>
	</div>

</body>
</html>