<!DOCTYPE html>
<html>

<head>
  <title>Formulir Pendaftaran</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    hr.new4 {
      border: 2px solid #000000;
    }
  </style>
</head>

<body>
  <h1>DATA SISWA BARU</h1>
  <a href="{{ URL::to('admin/cetak/datasiswaexcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
  <a href="{{ URL::to('admin/cetak/datasiswaexcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
  <table width="100%" border="1">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>ID Daftar</th>
        <th>JK</th>
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
        <th>Alamat</th>
        <th>Agama</th>
        <th>NISN</th>
        <th>Asal Sekolah</th>
        <th>No STTB</th>
        <th>No SKHU</th>
        <th>No Peserta Ujian SMP</th>
        <th>Nama Ayah</th>
        <th>Pendidikan Ayah</th>
        <th>Pekerjaan Ayah</th>
        <th>Nama Ibu</th>
        <th>Pendidikan Ibu</th>
        <th>Pekerjaan Ibu</th>
        <th>Alamat Orangtua</th>
        <th>Telp</th>
        <th>Jurusan 1</th>
        <th>Jurusan 2</th>
        <th>Status Bayar</th>
        <th>Status Lulus</th>
        <th>Tahun Ajaran</th>
      </tr>
    </thead>
    <tbody>
      @foreach($siswa as $item)
      <tr>
        <td align="center">{{ $no++ }}</td>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->id_daftar }}</td>
        <td>{{ $item->jk }}</td>
        <td>{{ $item->tempat_lahir }}</td>
        <td>{{ $item->tanggal_lahir }}</td>
        <td>{{ $item->alamat }}</td>
        <td>{{ $item->agama }}</td>
        <td align="center">{{ $item->nisn }}</td>
        <td>{{ $item->asal_sekolah }}</td>
        <td>{{ $item->nomor_sttb }}</td>
        <td>{{ $item->nomor_skhu }}</td>
        <td>{{ $item->nomor_peserta_ujian_smp }}</td>
        <td>{{ $item->ayah_nama }}</td>
        <td>{{ $item->ayah_pendidikan }}</td>
        <td>{{ $item->ayah_pekerjaan }}</td>
        <td>{{ $item->ibu_nama }}</td>
        <td>{{ $item->ibu_pendidikan }}</td>
        <td>{{ $item->ibu_pekerjaan }}</td>
        <td>{{ $item->alamat_orangtua }}</td>
        <td>{{ $item->telp }}</td>
        <td>{{ $item->jurusan_1 }}</td>
        <td>{{ $item->jurusan_2 }}</td>
        <td align="center">{{ $item->status_Bayar }}</td>
        <td align="center">{{ $item->status_lulus }}</td>
        <td>{{ $item->tahun_ajaran }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>

</html>