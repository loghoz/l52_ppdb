<!DOCTYPE html>
<html>
<head>
	<title>Formulir Pendaftaran</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        hr.new4 {
            border2px solid #000000;
        }
    </style>
    <style type="text/css" media="print">
    @page { 
        size: landscape;
    }
    body { 
        writing-mode: tb-rl;
    }
</style>
</head>
<body onload="window.print()">
		<table width="100%">
			<tbody>
				<tr>
					<td>
						<img src="{{asset('itlabil/image/default/logo.png')}}" width="150px">
					</td>
					<td align="center">
						<b><h3>FORMULIR PENDAFTARAN SISWA/I BARU</h3></b>
						<h5><font color="green">SEKOLAH MENENGAH KEJURUAN (SMK)</font><font color="red"> PELITA</font></h5>
						<h4>BISNIS MANAJEMEN, TEKNIK INFORMATIKA DAN KESEHATAN</h4>
						<h6>GEDONGTATAAN KABUPATEN PESAWARAN</h6>
						<h6>TAHUN PELAJARAN 2020/2021</h6>
					</td>
					<td>
						<img src="{{asset('itlabil/image/default/smk_bisa.png')}}" width="200px">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="col-md-12" align="center">
							<img src="{{asset('itlabil/image/default/4.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/3.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/5.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/2.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/6.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/7.png')}}" width="50px">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
        <hr class="new4">
		<table width="100%">
			<thead>
                <tr>
                    <th>NO</th>
                    <th>ID Daftar</th>
                    <th>Nama</th>
                    <th>Tempat & Tanggal Lahir</th>
                    <th>NISN</th>
                    <th>JK</th>
                    <th>Kelas</th>
                    <th>Agama</th>
                    <th>Asal Sekolah</th>
                    <th>Nomor STTB</th>
                    <th>Nomor SKHU</th>
                    <th>Nomor PU SMP</th>
                    <th>Alamat</th>
                    <th>Nama Ayah</th>
                    <th>Pendidikan Ayah</th>
                    <th>Pekerjaan Ayah</th>
                    <th>Nama Ibu</th>
                    <th>Pendidikan Ibu</th>
                    <th>Pekerjaan Ibu</th>
                    <th>Alamat Orangtua</th>
                    <th>Telp</th>
                    <th>Jurusan 1</th>
                    <th>Jurusan 2</th>
                    <th>Tgl Daftar</th>
                    <th>Status Bayar</th>
				</tr>
                @foreach($siswa as $data)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->id_daftar }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}</td>
                    <td>{{ $data->nisn }}</td>
                    <td>{{ $data->jk }}</td>
                    <td>{{ $data->kelas }}</td>
                    <td>{{ $data->agama }}</td>
                    <td>{{ $data->asal_sekolah }}</td>
                    <td>{{ $data->nomor_sttb }}</td>
                    <td>{{ $data->nomor_skhu }}</td>
                    <td>{{ $data->nomor_peserta_ujian_smp }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td>{{ $data->ayah_nama }}</td>
                    <td>{{ $data->ayah_pendidikan }}</td>
                    <td>{{ $data->ayah_pekerjaan }}</td>
                    <td>{{ $data->ibu_nama }}</td>
                    <td>{{ $data->ibu_pendidikan }}</td>
                    <td>{{ $data->ibu_pekerjaan }}</td>
                    <td>{{ $data->alamat_orangtua }}</td>
                    <td>{{ $data->telp }}</td>
                    <td>{{ $data->jurusan_1 }}</td>
                    <td>{{ $data->jurusan_2 }}</td>
                    <td>{{ $data->created_at->format('d/m/Y') }}</td>
                    <td>{{ $data->status_bayar }}</td>
				</tr>
                @endforeach
			</tbody>
		</table>
        <table width="100%">
			<tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="center">Gedongtataan, ...................................................</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="center">Yang Mendaftar</td>
                </tr>
                <tr height="80px">
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="center"><u>(......................................)</u></td>
                </tr>
            </tbody>
		</table>

</body>
</html>