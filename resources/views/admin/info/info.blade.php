@extends('layouts.admin')

@section('content')

<section class="content-header">
    <h1>
        Informasi
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Informasi</h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'admin.informasi.store', 'class'=>'form-horizontal','files'=>true])!!}
                    @include('form.tambah._admin_info')
                    {!! Form::close() !!}
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Informasi</th>
                                <th>Role</th>
                                <th>Photo</th>
                                <th>Publish</th>
                                <th>Tanggal</th>
                                <th>Ubah</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->informasi }}</td>
                                <td>{{ $item->role }}</td>
                                <td>@if($item->photo=="-")
                                    -
                                    @else
                                    <img src="{{url('itlabil/image/info')}}/{{$item->photo}}" alt="{{$item->photo}}" class="img-thumbnail" width="500px">
                                    @endif
                                </td>
                                <td>{{ $item->publish }}</td>
                                <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                <td><a href="{{ route('admin.informasi.edit', $item->id) }}" class="btn btn-primary">Ubah</a></td>
                                <td>
                                    {{ Form::open(['route' => ['admin.informasi.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                    {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection