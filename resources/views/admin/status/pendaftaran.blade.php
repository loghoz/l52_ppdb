@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Status Pendaftaran
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-body" style="overflow-x:auto;">
                        @foreach($data as $item)
                            <h2>Status pendaftaran saat ini : <strong>
                            @if($item->status_pendaftaran=="Aktif")
                                Di Buka
                            @else
                                Di Tutup
                            @endif
                            </strong></h2>
                            {!! Form::model($item->id, ['route' => ['admin.pendaftaran.update', $item->id],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                                {!! Form::submit('Ubah', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
