@extends('layouts.admin')

@section('content')

<section class="content-header">
    <h1>
        Jurusan
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Jurusan</h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'admin.jurusan.store', 'class'=>'form-horizontal','files'=>true])!!}
                    @include('form.tambah._admin_jurusan')
                    {!! Form::close() !!}
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Jurusan</th>
                                <th>Jurusan</th>
                                <th>Bidang Keahlian</th>
                                <th>Daftar Ulang</th>
                                <th>Info Jurusan</th>
                                <th>Publish</th>
                                <th>Ubah</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kode_jurusan }}</td>
                                <td>{{ $item->jurusan }}</td>
                                <td>{{ $item->bidang_keahlian }}</td>
                                <td>{!! $item->daftar_ulang !!}</td>
                                <td>{!! $item->info_jurusan !!}</td>
                                <td>{{ $item->publish }}</td>
                                <td><a href="{{ route('admin.jurusan.edit', $item->id) }}" class="btn btn-primary">Ubah</a></td>
                                <td>
                                    {{ Form::open(['route' => ['admin.jurusan.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                    {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection