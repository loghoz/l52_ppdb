@extends('layouts.admin')

@section('content')

<section class="content-header">
    <h1>
        Data Siswa Baru
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Siswa Baru</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>ID Daftar</th>
                                <th>JK</th>
                                <th>NISN</th>
                                <th>Asal Sekolah</th>
                                <th>Jurusan 1</th>
                                <th>Jurusan 2</th>
                                <th>Telp</th>
                                <th>Administrasi</th>
                                <th>Tgl Daftar</th>
                                <th>Tipe Daftar</th>
                                <th>Keterangan</th>
                                <th>Tahun Ajaran</th>
                                <th>Detail</th>
                                <th>Ubah</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($siswa as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->id_daftar }}</td>
                                <td>{{ $item->jk }}</td>
                                <td>{{ $item->nisn }}</td>
                                <td>{{ $item->asal_sekolah }}</td>
                                <td>{{ $item->jurusan_1 }}</td>
                                <td>{{ $item->jurusan_2 }}</td>
                                <td>{{ $item->telp }}</td>
                                @if($item->status_bayar=="Lunas")
                                <td><span class="badge bg-green">{{ $item->status_bayar }}</span></td>
                                @else
                                <td><span class="badge bg-red">{{ $item->status_bayar }}</span></td>
                                @endif
                                <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                <td>{{ $item->daftar }}</td>
                                <td>{{ $item->ket }}</td>
                                <td>{{ $item->tahun_ajaran }}</td>
                                <td><a href="{{ route('admin.siswa.show', $item->id) }}" class="btn btn-success">Lihat</a></td>
                                <td><a href="{{ route('admin.siswa.edit', $item->id) }}" class="btn btn-primary">Ubah</a></td>
                                <td>
                                    {{ Form::open(['route' => ['admin.siswa.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                    {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@endsection