@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Tambah Data Siswa Baru
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                {!! Form::open(['route' => 'admin.siswa.store', 'class'=>'form-horizontal','files'=>true])!!}
                    @include('form.tambah._admin_siswa')
                {!! Form::close() !!}

            </div>
        </div>
    </section>
@endsection
