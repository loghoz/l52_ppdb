@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Data Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                
                        {!! Form::model($siswa, ['route' => ['admin.siswa.update', $siswa],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form.ubah._admin_siswa', ['model' => $siswa])
                        {!! Form::close() !!}
         
            </div>
        </div>
    </section>
@endsection
