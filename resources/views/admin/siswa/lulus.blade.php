@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Status Penerimaan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-body" style="overflow-x:auto;">
                        @foreach($data as $item)
                            <h2>Status Penerimaan saat ini : <strong>
                            @if($item->status_pendaftaran=="Aktif")
                                Di Buka
                            @else
                                Di Tutup
                            @endif
                            </strong></h2>
                            {!! Form::model($item->id, ['route' => ['admin.pendaftaran.update', $item->id],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                                {!! Form::submit('Ubah', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        @endforeach
                        <hr>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-body" style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>ID Daftar</th>
                                    <th>JK</th>
                                    <th>Asal Sekolah</th>
                                    <th>Jurusan 1</th>
                                    <th>Jurusan 2</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Ket</th>
                                    <th>Status</th>
                                    <th>Ubah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswa as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->id_daftar }}</td>
                                        <td>{{ $item->jk }}</td>
                                        <td>{{ $item->asal_sekolah }}</td>
                                        <td>{{ $item->jurusan_1 }}</td>
                                        <td>{{ $item->jurusan_2 }}</td>
                                        <td>{{ $item->tahun_ajaran}}</td>
                                        <td>{{ $item->ket}}</td>
                                        @if($item->status_lulus=="-")
                                            <td><span class="badge bg-primary">Status Kosong</span></td>
                                        @elseif($item->status_lulus=="Lulus")
                                            <td><span class="badge bg-green">{{ $item->status_lulus }}</span></td>
                                        @else
                                            <td><span class="badge bg-red">{{ $item->status_lulus }}</span></td>
                                        @endif
                                        <td>
                                        {!! Form::model($item->id, ['route' => ['admin.infolulus.update', $item->id],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                                            {!! Form::submit('Ubah Status', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
