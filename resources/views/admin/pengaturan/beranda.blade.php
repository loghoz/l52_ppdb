@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Pengaturan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4">

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Tahun Ajaran Aktif : {{$tahun->meta_value}}</h3>
                    </div>
                    <hr>
                    <div class="box-body">
                        {!! Form::model($tahun, ['route' => ['admin.pengaturan.update', $tahun],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('meta_value') ? ' has-error' : '' }}">
                                    {!! Form::label('meta_value', 'Tahun Ajaran', ['class'=>'control-label col-sm-4']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('meta_value',$dataTa ,null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                    <small class="text-danger">{{ $errors->first('meta_value') }}</small>
                                    </div>
                                </div>
                                <div class="btn-group pull-right">
                                    {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Tahun Ajaran Daftar Aktif : {{$tadaf->meta_value}}</h3>
                    </div>
                    <hr>
                    <div class="box-body">
                        {!! Form::model($tadaf, ['route' => ['admin.pengaturan.update', $tadaf],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('meta_value') ? ' has-error' : '' }}">
                                    {!! Form::label('meta_value', 'Tahun Ajaran', ['class'=>'control-label col-sm-4']) !!}
                                    <div class="col-sm-8">
                                        {!! Form::select('meta_value',$dataTa ,null,array('class'=>'form-control')) !!}
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                    <small class="text-danger">{{ $errors->first('meta_value') }}</small>
                                    </div>
                                </div>
                                <div class="btn-group pull-right">
                                    {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
