@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            Kontak
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Kontak</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($kontak, ['route' => ['admin.kontak.update', $kontak],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form.ubah._admin_kontak', ['model' => $kontak])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
