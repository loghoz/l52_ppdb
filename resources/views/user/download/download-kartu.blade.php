<!DOCTYPE html>
<html>
<head>
	<title>Cetak Kartu</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<table width="100%">
			<tbody>
				<tr>
					<td>
						<img src="{{asset('itlabil/image/default/logo.png')}}" width="150px">
					</td>
					<td align="center">
						<b><h3>KARTU PENDAFTARAN SISWA/I BARU</h3></b>
						<h5><font color="green">SEKOLAH MENENGAH KEJURUAN (SMK)</font><font color="red"> PELITA</font></h5>
						<h4>BISNIS MANAJEMEN, TEKNIK INFORMATIKA DAN KESEHATAN</h4>
						<h6>GEDONGTATAAN KABUPATEN PESAWARAN</h6>
						<h6>TAHUN PELAJARAN 2020/2021</h6>
					</td>
					<td>
						<img src="{{asset('itlabil/image/default/smk_bisa.png')}}" width="200px">
					</td>
				</tr>
				<!-- <tr>
					<td colspan="3">
						<div class="col-md-12" align="center">
							<img src="{{asset('itlabil/image/default/4.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/3.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/5.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/2.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/6.png')}}" width="50px">
							<img src="{{asset('itlabil/image/default/7.png')}}" width="50px">
						</div>
					</td>
				</tr> -->
			</tbody>
		</table>
		<hr>
		<table class='table table-bordered'>
			<tbody>
                <tr>
                    <td width="300px">ID Daftar</td>
                    <td>: {{ $data->id_daftar }}</td>
                </tr>
                <tr>
                    <td>Nama</td>
            		<td>: {{ $data->nama }}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>: {{ $data->jk }}</td>
                </tr>
                <tr>
                    <td>Asal Sekolah</td>
                    <td>: {{ $data->asal_sekolah }}</td>
                </tr>
                <tr>
                    <td>NISN</td>
                    <td>: {{ $data->nisn }}</td>
                </tr>
                <tr>
                    <td>Tanggal Daftar</td>
                    <td>: {{ $data->created_at->format('d/m/Y') }}</td>
                </tr>
            </tbody>
		</table>
	</div>

</body>
</html>