@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">Kontak</li>
                    </ol>
                </div>
                <h4 class="page-title">Kontak</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/default/logo.png" width="150px" height="150px"><br><br>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-borderless mb-0">
                            <tbody>
                                @foreach($kontak as $item)
                                    <tr>
                                        <td>{{ $item->meta_key }}</td>
                                        @if('http'==str_limit($item->meta_value, $limit = 4, $end=''))
                                            <td>: <a href="{{ $item->meta_value }}">{{ $item->meta_value }}</a></td>
                                        @else
                                            <td>: {{ $item->meta_value }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="google-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.2347617359314!2d105.09360638754993!3d-5.381137437987164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e40d26c3ac754a1%3A0x15f043e9bf47fa24!2sSMK%20Pelita%20Gedong%20Tataan!5e0!3m2!1sen!2sid!4v1585557860096!5m2!1sen!2sid" width="100%" height="500px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
</div>
@endsection
