@extends('layouts.app')

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <div class="btn-group pull-right">
          <ol class="breadcrumb hide-phone p-0 m-0">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item active">Cek</li>
            <li class="breadcrumb-item">Persyaratan</li>
          </ol>
        </div>
        <h4 class="page-title">Persyaratan</h4>
      </div>
    </div>
  </div>

  <div class="row">
    @foreach($data as $item)
    <div class="col-md-12">
      <h4 class="header-title m-t-0 m-b-30">
        @if($item->ket=="pas-photo")
        Pas Photo
        @elseif($item->ket=="kk")
        Kartu Keluarga
        @elseif($item->ket=="skl")
        Surat Keterangan Lulus
        @elseif($item->ket=="rapot")
        Rapot
        @else
        @endif
      </h4>
    </div>
    <div class="col-md-12">
      <img src="{{url('itlabil/image/persyaratan')}}/{{$item->photo}}" alt="{{$item->photo}}" class="img-thumbnail" width="800px"><br><br>
    </div>
    <div class="col-md-12">
      {{ Form::open(['route' => ['persyaratan.destroy' , $item->id] ,'method' => 'DELETE']) }}
      {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
      {{ Form::close() }}<br>
    </div>
    @endforeach
  </div>
  <a href="/status" class="btn btn-custom">Kembali</a>
</div>

@endsection