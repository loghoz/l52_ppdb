@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item active">infoLulus</li>
                        <li class="breadcrumb-item">Cek</li>
                    </ol>
                </div>
                <h4 class="page-title">infoLulus</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/default/logo.png" width="200px" height="200px"><br><br>
                        <font size="4px">MASUKKAN NOMOR ID DAFTAR</font><br><br>
                    </div>
                    <div class="col-md-12" align="center">
                        {!! Form::open(['route' => 'status.store','class'=>'form-horizontal'])!!}
                        <div class="form-group{{ $errors->has('id_daftar') ? ' has-error' : '' }}">
                            {!! Form::text('id_daftar', null, ['class' => 'form-control','placeholder'=>'ID DAFTAR']) !!}
                            <small class="text-danger">{{ $errors->first('id_daftar') }}</small>
                        </div>

                        <div class="form-group" align="center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-custom">
                                    Cek Status
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

</div>

@endsection