@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item">Status</li>
                    </ol>
                </div>
                <h4 class="page-title">Status</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <div class="row">

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card m-b-30 text-white bg-danger text-xs-center">
                                    <div class="card-body">
                                        <blockquote class="card-bodyquote">
                                            <center>
                                                <h4><strong>Status Pembayaran</strong></h4>
                                            </center>
                                            <center>
                                                <h3><strong><u>{{$siswa->status_bayar}}</u></strong></h3>
                                            </center>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card m-b-30 text-white bg-success text-xs-center">
                                    <div class="card-body">
                                        <blockquote class="card-bodyquote">
                                            <center>
                                                <h4><strong>Status Penerimaan</strong></h4>
                                            </center>
                                            @if($stt_pen->status_pendaftaran=="Aktif")
                                            <center>
                                                <h3><strong>{{$siswa->status_lulus}}</strong></h3>
                                            </center>
                                            @else
                                            <center>
                                                <h3><strong>INFO DITUTUP</strong></h3>
                                            </center>
                                            @endif
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card m-b-30 text-white bg-pink text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <center>
                                        <h4><strong>Note</strong></h4>
                                    </center>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Data Pendaftar</td>
                                                @if($siswa->img_ijazah=="-"||$siswa->img_skhu=="-"||$siswa->nomor_sttb==""||$siswa->nomor_skhu==""||$siswa->nomor_peserta_ujian_smp==""||$siswa->ayah_nama==""||$siswa->ibu_nama=="")
                                                <td>
                                                    Belum Lengkap : <br>
                                                    @if($siswa->nomor_sttb=="")
                                                    - Nomor STTB/Ijazah Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->nomor_skhu=="")
                                                    - Nomor SKHU Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->nomor_peserta_ujian_smp=="")
                                                    - Nomor Peserta Ujian SMP Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->ayah_nama=="")
                                                    - Nama Ayah Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->ayah_nama!="")
                                                    @if($siswa->ayah_pendidikan=="-")
                                                    - Pilih Status Pendidikan Ayah<br>
                                                    @endif
                                                    @if($siswa->ayah_pekerjaan=="-")
                                                    - Pilih Status Pekerjaan Ayah<br>
                                                    @endif
                                                    @endif
                                                    @if($siswa->ibu_nama=="")
                                                    - Nama Ibu Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->ibu_nama!="")
                                                    @if($siswa->ibu_pendidikan=="-")
                                                    - Pilih Status Pendidikan Ibu<br>
                                                    @endif
                                                    @if($siswa->ibu_pekerjaan=="-")
                                                    - Pilih Status Pekerjaan Ibu<br>
                                                    @endif
                                                    @endif
                                                    @if($siswa->alamat_orangtua=="")
                                                    - Alamat Orang Tua Masih Kosong<br>
                                                    @endif
                                                    @if($siswa->img_ijazah=="-")
                                                    - Upload Foto STTB/Ijazah<br>
                                                    @endif
                                                    @if($siswa->img_skhu=="-")
                                                    - Upload Foto SKHU
                                                    @endif
                                                </td>
                                                @else
                                                <td>: Lengkap</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>Berkas</td>
                                                <td>
                                                    Berkas yang wajib dikumpulkan secara langsung :<br>
                                                    a) Foto Copy STTB / Ijazah : 2 Lembar<br>
                                                    b) Foto Copy SKHU : 2 Lembar<br>
                                                    c) Pas Photo 3x4 : 4 Lembar<br>
                                                    d) Pas Photo 2x3 : 2 Lembar
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <footer>
                                        <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#ffffff;"><b>Admin PPDB</b></a> jika anda butuh bantuan.</cite>
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                        <div class="card m-b-30 text-white bg-info text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <center>
                                        <h4><strong>Info</strong></h4>
                                    </center>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Tanggal Daftar</td>
                                                <td>: {{ $siswa->created_at->format('d/m/Y') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @foreach($info as $item)
                                    <hr>
                                    <p>Tanggal : {{ $item->created_at->format('d/m/Y') }}</p>
                                    @if($item->photo=="-")
                                    @else
                                    <img src="{{url('itlabil/image/info')}}/{{$item->photo}}" alt="{{$item->photo}}" class="img-thumbnail" width="400px">
                                    @endif
                                    <p>{{ $item->informasi }}</p>
                                    @endforeach
                                </blockquote>
                            </div>
                        </div>

                        <div class="card m-b-30 text-white bg-purple text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <center>
                                        <h4><strong>Download</strong></h4>
                                    </center>
                                    <table class="table table-borderless mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Kartu Pendaftaran</td>
                                                <td>: <a href="/pdfkartudaftar/{{$siswa->id}}" target="_blank" style="color:#ffffff;">Download</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card m-b-30 text-white bg-custom text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <center>
                                        <h4><strong>Data Pendaftar</strong></h4>
                                    </center>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>ID Daftar</td>
                                                <td>: {{$siswa->id_daftar}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama</td>
                                                <td>: {{$siswa->nama}}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td>: {{$siswa->jk}}</td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Tanggal Lahir</td>
                                                <td>: {{$siswa->tempat_lahir}}, {{$siswa->tanggal_lahir}}</td>
                                            </tr>
                                            <tr>
                                                <td>Agama</td>
                                                <td>: {{$siswa->agama}}</td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td>: {{$siswa->alamat}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Telp / HP</td>
                                                <td>: {{$siswa->telp}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kelas</td>
                                                <td>: {{$siswa->kelas}}</td>
                                            </tr>
                                            <tr>
                                                <td>NISN</td>
                                                <td>: {{$siswa->nisn}}</td>
                                            </tr>
                                            <tr>
                                                <td>Asal Sekolah</td>
                                                <td>: {{$siswa->asal_sekolah}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor STTB/Ijazah</td>
                                                <td>: {{$siswa->nomor_sttb}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor SKHU</td>
                                                <td>: {{$siswa->nomor_skhu}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Peserta Ujian SMP</td>
                                                <td>: {{$siswa->nomor_peserta_ujian_smp}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>Jurusan Yang dipilih</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Jurusan 1</td>
                                                <td>: {{$siswa->jurusan_1}}</td>
                                            </tr>
                                            <tr>
                                                <td>Jurusan 2</td>
                                                <td>: {{$siswa->jurusan_2}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>Data Orang Tua</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="200px">Nama Ayah</td>
                                                <td>: {{$siswa->ayah_nama}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan Ayah</td>
                                                <td>: {{$siswa->ayah_pekerjaan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan Ayah</td>
                                                <td>: {{$siswa->ayah_pendidikan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Ibu</td>
                                                <td>: {{$siswa->ibu_nama}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan Ibu</td>
                                                <td>: {{$siswa->ibu_pekerjaan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan Ibu</td>
                                                <td>: {{$siswa->ibu_pendidikan}}</td>
                                            </tr>
                                            <tr>
                                                <td>Alamat Orangtua</td>
                                                <td>: {{$siswa->alamat_orangtua}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>STTB/Ijazah & SKHU</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    @if($siswa->img_ijazah=="-")
                                                    - Belum Upload Gambar
                                                    @else
                                                    - Sudah Upload Gambar
                                                    @endif
                                                    <br>
                                                    @if($siswa->img_skhu=="-")
                                                    - Belum Upload Gambar
                                                    @else
                                                    - Sudah Upload Gambar
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <footer>
                                        <cite title="Source Title">*Cek Data Kamu Klik Tombol Ubah Jika ada yang salah dan Lengkapi data jika yang masih kosong </cite><br><br>
                                        <a href="{{ route('daftar.edit', $siswa->id) }}" class="btn btn-warning">Ubah</a>
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                        <h4 class="header-title m-t-0 m-b-30">Form Persyaratan</h4>
                        <div id="accordion" role="tablist" aria-multiselectable="true" class="m-b-30">
                            <div class="card bg-warning">
                                <div class="card-header bg-warning" role="tab" id="headingOne">
                                    <h5 class="mb-0 mt-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseHead" class="text-dark collapsed" aria-expanded="false" aria-controls="collapseOne">
                                            Persyaratan
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapseHead" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                    <div class="card-body">
                                        <p>
                                            - Pas Photo : {{$spas}} Terupload<br>
                                            - Surat Keterangan Lulus : {{$sskl}} Terupload<br>
                                            - Rapot : {{$srapot}} Terupload<br>
                                            - Kartu Keluarga : {{$skk}} Terupload
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-custom">
                                <div class="card-header bg-custom" role="tab" id="headingOne">
                                    <h5 class="mb-0 mt-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="text-white collapsed" aria-expanded="false" aria-controls="collapseOne">
                                            Upload Pas Foto Berwarna
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                    <div class="card-body">
                                        {!! Form::open(['route' => 'persyaratan.store','class'=>'form-horizontal','files'=>true])!!}
                                        <div class="card-box">
                                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }} row">
                                                <label class="col-4 col-form-label">Pas Foto</label>
                                                {!! Form::file('photo') !!}
                                                <label class="col-4"></label>
                                                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('photo') }}</small>
                                            </div>
                                            {!! Form::hidden('ket', 'pas-photo', ['class' => 'col-8 form-control']) !!}
                                            {!! Form::hidden('siswa_id', $siswa->id, ['class' => 'col-8 form-control']) !!}
                                            <button type="submit" class="btn btn-custom">
                                                Upload
                                            </button>
                                        </div>
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                            <div class="card bg-danger">
                                <div class="card-header bg-danger" role="tab" id="headingOne">
                                    <h5 class="mb-0 mt-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="text-white collapsed" aria-expanded="false" aria-controls="collapseOne">
                                            Upload Foto/Scan Surat Keterangan Lulus
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                    <div class="card-body">
                                        {!! Form::open(['route' => 'persyaratan.store','class'=>'form-horizontal','files'=>true])!!}
                                        <div class="card-box">
                                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }} row">
                                                <label class="col-4 col-form-label">Pas Foto</label>
                                                {!! Form::file('photo') !!}
                                                <label class="col-4"></label>
                                                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('photo') }}</small>
                                            </div>
                                            {!! Form::hidden('ket', 'skl', ['class' => 'col-8 form-control']) !!}
                                            {!! Form::hidden('siswa_id', $siswa->id, ['class' => 'col-8 form-control']) !!}
                                            <button type="submit" class="btn btn-danger">
                                                Upload
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-success">
                                <div class="card-header bg-success" role="tab" id="headingOne">
                                    <h5 class="mb-0 mt-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="text-white collapsed" aria-expanded="false" aria-controls="collapseOne">
                                            Upload Foto/Scan Rapot Semester 1-5
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                    <div class="card-body">
                                        {!! Form::open(['route' => 'persyaratan.store','class'=>'form-horizontal','files'=>true])!!}
                                        <div class="card-box">
                                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }} row">
                                                <label class="col-4 col-form-label">Pas Foto</label>
                                                {!! Form::file('photo') !!}
                                                <label class="col-4"></label>
                                                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('photo') }}</small>
                                            </div>
                                            {!! Form::hidden('ket', 'rapot', ['class' => 'col-8 form-control']) !!}
                                            {!! Form::hidden('siswa_id', $siswa->id, ['class' => 'col-8 form-control']) !!}
                                            <button type="submit" class="btn btn-success">
                                                Upload
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="card bg-pink">
                                <div class="card-header bg-pink" role="tab" id="headingOne">
                                    <h5 class="mb-0 mt-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="text-white collapsed" aria-expanded="false" aria-controls="collapseOne">
                                            Upload Foto/Scan Kartu Keluarga
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                    <div class="card-body">
                                        {!! Form::open(['route' => 'persyaratan.store','class'=>'form-horizontal','files'=>true])!!}
                                        <div class="card-box">
                                            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }} row">
                                                <label class="col-4 col-form-label">Pas Foto</label>
                                                {!! Form::file('photo') !!}
                                                <label class="col-4"></label>
                                                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('photo') }}</small>
                                            </div>
                                            {!! Form::hidden('ket', 'kk', ['class' => 'col-8 form-control']) !!}
                                            {!! Form::hidden('siswa_id', $siswa->id, ['class' => 'col-8 form-control']) !!}
                                            <button type="submit" class="btn btn-pink">
                                                Upload
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection