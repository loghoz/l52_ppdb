@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                    </ol>
                </div>
                <h4 class="page-title">Beranda</h4>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/default/logo.png" width="200px" height="200px"><br><br>
                        <font size="4px">WEBSITE PENDAFTARAN PESERTA DIDIK BARU</font><br>
                        <font size="5px"><b>SMK PELITA PESAWARAN</b></font><br>
                        <font size="4px">PROVINSI LAMPUNG</font><br><br>
                    </div>
                    <div class="col-md-12" align="center">
                        <img src="itlabil/image/default/4.png" width="50px">
                        <img src="itlabil/image/default/3.png" width="50px">
                        <img src="itlabil/image/default/5.png" width="50px">
                        <img src="itlabil/image/default/2.png" width="50px">
                        <img src="itlabil/image/default/6.png" width="50px">
                        <img src="itlabil/image/default/7.png" width="50px">
                    </div>
                    <div class="col-md-12" align="center"><br>
                        <a href="daftar" class="btn btn-custom">Form Pendaftaran</a>
                        <a href="cekdaftar" class="btn btn-custom">Cek Daftar</a>
                        <a href="cekstatus" class="btn btn-custom">Cek Status</a>
                    </div>
                    <div class="col-md-12" align="center"><br>
                        Untuk Info Kunjungi Website Resmi SMK Pelita Pesawaran<br>
                        <a href="http://smkpelitapesawaran.sch.id" style="color:#000000;" class="btn btn-custom">Disini</a><br>
                        Bagi yang sudah mendaftar bisa JOIN Group OFFICIAL CASIS SMKPELITA
                        untuk mendapatkan Informasi lebih lanjut.<br>
                        <a href="https://chat.whatsapp.com/CmMxbRzSM7u4JsPTcAcqON" style="color:#000000;" class="btn btn-custom">Join Disini</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection