@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item">Daftar</li>
                    </ol>
                </div>
                <h4 class="page-title">Daftar</h4>
            </div>
        </div>
    </div>

    @foreach($stt_daftar as $item)
    
        @if($item->status_pendaftaran=="Aktif")
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">

                        <div class="row">
                            <div class="col-md-12" align="center">
                                <font size="4px">Isi Form Pendaftaran</font><br><br>
                            </div>
                            <div class="col-md-12">
                                {!! Form::open(['route' => 'daftar.store','class'=>'form-horizontal'])!!}
                                    @include('form.tambah._daftar_tambah')
                                {!! Form::close() !!}
                            </div>
                            <footer>
                                <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#02c0ce;"><b>Admin PPDB</b></a> jika anda butuh bantuan dalam proses pendaftaran.</cite>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">

                        <div class="row">
                            <div class="col-md-12" align="center">
                            <br><br><br><font size="8px" >PENDAFTARAN DI TUTUP</font><br><br>
                            
                            <footer>
                                <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#02c0ce;"><b>Admin PPDB</b></a> jika anda butuh bantuan dalam proses pendaftaran.</cite>
                            </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @endforeach

</div>

@endsection
