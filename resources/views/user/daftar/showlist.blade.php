@extends('layouts.app')

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <div class="btn-group pull-right">
          <ol class="breadcrumb hide-phone p-0 m-0">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item">Cek Daftar</li>
          </ol>
        </div>
        <h4 class="page-title">Hasil Cek Daftar</h4>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card-box">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">

            @foreach($siswa as $item)
            <div class="card m-b-30 text-white bg-custom text-xs-center">
              <div class="card-body">
                <blockquote class="card-bodyquote">
                  <center><h3>Hasil Pengecekan</h3>
                  <hr>
                  <h4>{{$item->nama}}</h4>
                  <h5>{{$item->asal_sekolah}}</h5>
                  <h5>
                    @if($item->status_lulus=="Lulus")
                    Diterima di Jurusan {{$item->jurusan_1}}
                    @elseif($item->status_lulus=="Tidak Lulus")
                    *Hubungi <b>Admin SIPO (0853 6980 7844)</b>.
                    @elseif($item->status_lulus=="-")
                    *Hubungi <b>Admin SIPO (0853 6980 7844)</b>.
                    @else
                    *Hubungi <b>Admin SIPO (0853 6980 7844)</b>.
                    @endif
                  </h5></center>
                  <hr>
                  @foreach($jurusan as $data)
                  @if($item->jurusan_1==$data->jurusan)
                  Daftar Ulang :<br>
                  {!! $data->daftar_ulang!!}
                  @else
                  @endif
                  @endforeach
                  <hr>
                  @foreach($jurusan as $data)
                  @if($item->jurusan_1==$data->jurusan)
                  Info :<br>
                  {!! $data->info_jurusan!!}
                  @else
                  @endif
                  @endforeach
                  <hr>
                  <footer>
                    <cite title="Source Title">*Hubungi <b>Admin SIPO (0853 6980 7844)</b> jika Kamu butuh bantuan.</cite>
                  </footer>
                </blockquote>
              </div>
            </div>

            @endforeach
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </div>
  </div>

  @endsection