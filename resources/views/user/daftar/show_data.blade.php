@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item">Kartu Daftar</li>
                    </ol>
                </div>
                <h4 class="page-title">Kartu Daftar</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="card m-b-30 text-white bg-custom text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <center><h4><strong>KARTU DAFTAR</strong></h4></center>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td width="100px">ID Daftar</td>
                                                <td>: {{ $siswa->id_daftar }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama</td>
                                                <td>: {{ $siswa->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td>: {{ $siswa->jk }}</td>
                                            </tr>
                                            <tr>
                                                <td>Asal Sekolah</td>
                                                <td>: {{ $siswa->asal_sekolah }}</td>
                                            </tr>
                                            <tr>
                                                <td>NISN</td>
                                                <td>: {{ $siswa->nisn }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Daftar</td>
                                                <td>: {{ $siswa->created_at->format('d/m/Y') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <footer>
                                        <cite title="Source Title">*Screen Shot Kartu ini sebagai bukti pendaftaran</cite>
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <a href="cekstatus" class="btn btn-custom">Cek Status</a>
                    </div>
                    <!-- <div class="col-md-2" align="right">
                        <a href="bayardaftar" class="btn btn-custom">Lanjut Pembayaran</a>
                    </div> -->
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection