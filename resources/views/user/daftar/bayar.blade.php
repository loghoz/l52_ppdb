@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item">Bayar Pendaftaran</li>
                    </ol>
                </div>
                <h4 class="page-title">Bayar Pendaftaran</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-12" align="center">
                        <font size="4px">Bayar Pendaftaran</font><br><br>
                        Pembayaran pendaftaran dapat dilakukan dengan beberapa cara, ikuti langkah - langkah dibawah ini!<br><br>
                    </div>
                    <div class="col-md-12">
                        <div class="card m-b-30 text-white bg-custom text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <h4>1. Langsung</h4>
                                    <p>
                                        a) Siapkan uang sebesar <b>Rp. 30.000.</b><br>
                                        b) Setorkan ke Admin PPDB di SMK Pelita Pesawaran.<br>
                                        c) Simpan Bukti Pembayaran.<br>
                                        d) Cek Status Pembayaran di Website PPDB SMK Pelita Pesawaran.<br>
                                        e) Masuk Menu Cek Status.<br>
                                        f) Masukkan Code <b>ID Daftar</b>, Lanjut Klik Cek Status.<br>
                                        g) Cek Status Pembayaran di Kolom Status Pembayaran yang berwarna Merah.<br>
                                        h) Jika Status Pembayaran <b>"Belum Lunas"</b>, Tunggu 1x24 Jam.<br>
                                        i) Jika Status masih sama , Hub Admin PPDB. Info <a href="/kontak" style="color:#ffffff;"><b>Disini</b></a>.<br>
                                        j) Jika Status Pembayaran <b>"Lunas"</b> maka proses telah <b>SELESAI</b>.
                                    </p>
                                    <footer>
                                        <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#ffffff;"><b>Admin PPDB</b></a> jika anda butuh bantuan dalam proses pembayaran.</cite>
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                        <div class="card m-b-30 text-white bg-info text-xs-center">
                            <div class="card-body">
                                <blockquote class="card-bodyquote">
                                    <h4>2. Melalui Aplikasi Dana</h4>
                                    <p>
                                        a) Buka Aplikasi Dana di Handphone Kamu.<br>
                                        b) Pastikan Terdapat Saldo Sebesar Rp. 30.000. <br>
                                        c) Masuk Menu Kirim / Send. Dan pilih Kirim Ke Nomor Telpon/Send To Phone Number.<br>
                                        d) Masukkan Nomor Telpon Pembayaran ({{$kontak1->meta_value}} / {{$kontak2->meta_value}}).<br>
                                        e) Masukkan Jumlah Pembayaran sebesar Rp.30.000.<br>
                                        f) Klik Tentukan Jumlah / Set Amount.<br>
                                        g) Pada Note/Pesan Ketikan ID Daftar Kamu. Untuk mempermudah Admin PPDB Dalam Pengecekan Pembayaran yang telah masuk.<br>
                                        h) Klik Kirim Uang / Send Money.<br>
                                        i) Jangan Lupa Screen Shot Hasil Pengiriman Sebagai Bukti telah membayar pendaftaran.<br>
                                        j) Konfirmasi pembayaran pada Admin PPDB via Telpon / Whatsapp ({{$kontak1->meta_value}} / {{$kontak2->meta_value}}).<br>
                                        k) Cek Status Pembayaran di Website PPDB SMK Pelita Pesawaran.<br>
                                        l) Masuk Menu Cek Status.<br>
                                        m) Masukkan Code <b>ID Daftar</b>, Lanjut Klik Cek Status.<br>
                                        n) Cek Status Pembayaran di Kolom Status Pembayaran yang berwarna Merah.<br>
                                        o) Jika Status Pembayaran <b>"Belum Lunas"</b>, Tunggu 1x24 Jam.<br>
                                        p) Jika Status masih sama , Hub Admin PPDB. Info <a href="/kontak" style="color:#ffffff;"><b>Disini</b></a>.<br>
                                        q) Jika Status Pembayaran <b>"Lunas"</b> maka proses telah <b>SELESAI</b>.
                                    </p>
                                    <footer>
                                        <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#ffffff;"><b>Admin PPDB</b></a> jika anda butuh bantuan dalam proses pembayaran.</cite>
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
