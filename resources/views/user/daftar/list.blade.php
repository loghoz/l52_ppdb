@extends('layouts.app')

@section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <div class="btn-group pull-right">
          <ol class="breadcrumb hide-phone p-0 m-0">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item active">Pendaftaran</li>
            <li class="breadcrumb-item">Cek</li>
          </ol>
        </div>
        <h4 class="page-title">Cek Pendaftaran</h4>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <div class="card-box">

        <div class="row">
          <div class="col-md-12" align="center">
            <img src="itlabil/image/default/logo.png" width="200px" height="200px"><br><br>
            <font size="4px">MASUKKAN NAMA DAFTAR</font><br><br>
          </div>
          <div class="col-md-12" align="center">
            {!! Form::open(['route' => 'cekdaftar.store','class'=>'form-horizontal'])!!}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Daftar']) !!}
              <small class="text-danger">{{ $errors->first('nama') }}</small>
            </div>

            <div class="form-group" align="center">
              <div class="col-md-12">
                <button type="submit" class="btn btn-custom">
                  Cek Daftar
                </button>
              </div>
            </div>
            {!! Form::close() !!}
            <cite title="Source Title">*Hubungi <b>Admin SIPO (0853 6980 7844)</b> jika Kamu butuh bantuan.</cite>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>

@endsection