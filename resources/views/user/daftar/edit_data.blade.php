@extends('layouts.app')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                        <li class="breadcrumb-item">Daftar</li>
                    </ol>
                </div>
                <h4 class="page-title">Daftar</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">

                <div class="row">
                    <div class="col-md-12" align="center">
                        <font size="4px">Ubah Data</font><br><br>
                    </div>
                    <div class="col-md-12">
                        {!! Form::model($siswa, ['route' => ['daftar.update', $siswa],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form.ubah._daftar_ubah', ['model' => $siswa])
                        {!! Form::close() !!}
                    </div>
                    <footer>
                        <cite title="Source Title">*Hubungi <a href="/kontak" style="color:#02c0ce;"><b>Admin PPDB</b></a> jika anda butuh bantuan dalam proses pendaftaran.</cite>
                    </footer>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
