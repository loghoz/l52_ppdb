@extends('layouts.ppdb')

@section('content')

<section class="content-header">
  <h1>
    Show Data Persyaratan
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box box-danger">
        <div class="box-body">
          {!! Form::open(['route' => 'ppdb.persyaratan.store', 'class'=>'form-horizontal','files'=>true])!!}
          @include('form.tambah._admin_syarat')
          {!! Form::close() !!}
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          @foreach($data as $syarat)
          @if($syarat->ket=="pas-photo")
          Pas Photo<br><br>
          <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
          {{ Form::open(['route' => ['ppdb.persyaratan.destroy' , $syarat->id] ,'method' => 'DELETE']) }}
          {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
          {{ Form::close() }}<br><br>
          @endif
          @endforeach
          <hr>
          @foreach($data as $syarat)
          @if($syarat->ket=="skl")
          Surat Keterangan Lulus<br><br>
          <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
          {{ Form::open(['route' => ['ppdb.persyaratan.destroy' , $syarat->id] ,'method' => 'DELETE']) }}
          {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
          {{ Form::close() }}<br><br>
          @endif
          @endforeach
          <hr>

          @foreach($data as $syarat)
          @if($syarat->ket=="rapot")
          Rapot<br><br>
          <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
          {{ Form::open(['route' => ['ppdb.persyaratan.destroy' , $syarat->id] ,'method' => 'DELETE']) }}
          {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
          {{ Form::close() }}<br><br>
          @endif
          @endforeach
          <hr>

          @foreach($data as $syarat)
          @if($syarat->ket=="kk")
          Kartu Keluarga<br><br>
          <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
          {{ Form::open(['route' => ['ppdb.persyaratan.destroy' , $syarat->id] ,'method' => 'DELETE']) }}
          {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
          {{ Form::close() }}<br><br>
          @endif
          @endforeach
        </div>
      </div>

    </div>
  </div>
  <a href="{{ route('ppdb.persyaratan.index') }}" class="btn btn-primary">Kembali</a>
</section>
@endsection