@extends('layouts.ppdb')

@section('content')

<section class="content-header">
    <h1>
        Show Data Siswa - {{ $siswa->nama }}
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-striped">
                        <tr>
                            <td width="200px">ID Daftar</td>
                            <td>: {{$siswa->id_daftar}}</td>
                        </tr>
                        <tr>
                            <td width="200px">Jenis Kelamin</td>
                            <td>: {{$siswa->jk}}</td>
                        </tr>
                        <tr>
                            <td>Tempat Tanggal Lahir</td>
                            <td>: {{$siswa->tempat_lahir}}, {{$siswa->tanggal_lahir}}</td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td>: {{$siswa->agama}}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>: {{$siswa->alamat}}</td>
                        </tr>
                        <tr>
                            <td>Nomor Telp</td>
                            <td>: {{$siswa->telp}}</td>
                        </tr>
                        <tr>
                            <td>Kelas</td>
                            <td>: {{$siswa->kelas}}</td>
                        </tr>
                        <tr>
                            <td>NISN</td>
                            <td>: {{$siswa->nisn}}</td>
                        </tr>
                        <tr>
                            <td>Asal Sekolah</td>
                            <td>: {{$siswa->asal_sekolah}}</td>
                        </tr>
                        <tr>
                            <td>Nomor STTB/Ijazah</td>
                            <td>: {{$siswa->nomor_sttb}}</td>
                        </tr>
                        <tr>
                            <td>Nomor SKHU</td>
                            <td>: {{$siswa->nomor_skhu}}</td>
                        </tr>
                        <tr>
                            <td>Nomor Peserta Ujian SMP</td>
                            <td>: {{$siswa->nomor_peserta_ujian_smp}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Jurusan Yang dipilih</strong></td>
                        </tr>
                        <tr>
                            <td>Jurusan 1</td>
                            <td>: {{$siswa->jurusan_1}}</td>
                        </tr>
                        <tr>
                            <td>Jurusan 2</td>
                            <td>: {{$siswa->jurusan_2}}</td>
                        </tr>
                        <tr>
                            <td>Tahun Ajaran</td>
                            <td>: {{$siswa->tahun_ajaran}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Status Bayar Pendaftaran</strong></td>
                        </tr>
                        <tr>
                            <td>Administrasi</td>
                            @if($siswa->status_bayar=="Lunas")
                            <td>: <span class="badge bg-green">{{ $siswa->status_bayar }}</span></td>
                            @else
                            <td>: <span class="badge bg-red">{{ $siswa->status_bayar }}</span></td>
                            @endif
                        </tr>
                        <tr>
                            <td>Tipe Daftar</td>
                            @if($siswa->daftar=="Online")
                            <td>: <span class="badge bg-green">{{ $siswa->daftar }}</span></td>
                            @else
                            <td>: <span class="badge bg-red">{{ $siswa->daftar }}</span></td>
                            @endif
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>: {{$siswa->ket}}</td>
                        </tr>
                        <tr>
                            <td>Info Siswa</td>
                            <td>: {{$siswa->info}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Orang Tua</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-striped">
                        <tr>
                            <td width="200px">Nama Ayah</td>
                            <td>: {{$siswa->ayah_nama}}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ayah</td>
                            <td>: {{$siswa->ayah_pekerjaan}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ayah</td>
                            <td>: {{$siswa->ayah_pendidikan}}</td>
                        </tr>
                        <tr>
                            <td>Nama Ibu</td>
                            <td>: {{$siswa->ibu_nama}}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ibu</td>
                            <td>: {{$siswa->ibu_pekerjaan}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ibu</td>
                            <td>: {{$siswa->ibu_pendidikan}}</td>
                        </tr>
                        <tr>
                            <td>Alamat Orangtua</td>
                            <td>: {{$siswa->alamat_orangtua}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Kelengkapan</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    Gambar Ijazah : <br>
                    @if($siswa->img_ijazah=="-")
                    Belum Upload Gambar
                    @else
                    <img src="{{url('itlabil/image/image_user')}}/{{$siswa->img_ijazah}}" alt="{{$siswa->img_ijazah}}" class="img-thumbnail" width="600px"><br><br>
                    {!! Form::model($siswa, ['route' => ['admin.persyaratan.update', $siswa],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                    @include('form.ubah._admin_ijazah', ['model' => $siswa])
                    {!! Form::close() !!}
                    @endif
                    <br><br><br>
                    Gambar SKHU : <br>
                    @if($siswa->img_skhu=="-")
                    Belum Upload Gambar
                    @else
                    <img src="{{url('itlabil/image/image_user')}}/{{$siswa->img_skhu}}" alt="{{$siswa->img_skhu}}" class="img-thumbnail" width="600px"><br><br>
                    {!! Form::model($siswa, ['route' => ['admin.persyaratan.update', $siswa],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                    @include('form.ubah._admin_skhu', ['model' => $siswa])
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <a href="{{ route('ppdb.siswa.index') }}" class="btn btn-primary">Kembali</a>
</section>
@endsection