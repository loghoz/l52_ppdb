@extends('layouts.ppdb')

@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section><br><br>

    <div class="row">
        <div class="col-sm-6 col-md-6">
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Catatan Informasi</h3>
                </div>

                <div class="box-body scroller">
                    @foreach($info as $item)
                    @if($item->publish=="Tampil")
                    Tanggal : {{ $item->created_at->format('d/m/Y') }}<br>
                    @if($item->photo=="-")
                    @else
                    <img src="{{url('itlabil/image/info')}}/{{$item->photo}}" alt="{{$item->photo}}" class="img-thumbnail" width="500px"><br><br>
                    @endif
                    Catatan : {{$item->informasi}}
                    <hr>
                    @else
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="col-md-12">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Penerimaan</h3>
                            </div>

                            <div class="box-body">
                                - Jumlah Siswa : {{$jmlsemua}} Orang
                                <hr>
                                <span class="badge bg-green">{{ $l_ap }}</span>
                                <span class="badge bg-red">{{ $tl_ap }}</span>
                                <span class="badge bg-primary">{{ $k_ap }}</span> - Administrasi Perkantoran<br>
                                <span class="badge bg-green">{{ $l_ak }}</span>
                                <span class="badge bg-red">{{ $tl_ak }}</span>
                                <span class="badge bg-primary">{{ $k_ak }}</span> - Akuntansi<br>
                                <span class="badge bg-green">{{ $l_pms }}</span>
                                <span class="badge bg-red">{{ $tl_pms }}</span>
                                <span class="badge bg-primary">{{ $k_pms }}</span> - Pemasaran<br>
                                <span class="badge bg-green">{{ $l_tkj }}</span>
                                <span class="badge bg-red">{{ $tl_tkj }}</span>
                                <span class="badge bg-primary">{{ $k_tkj }}</span> - Teknik Komputer & Jaringan<br>
                                <span class="badge bg-green">{{ $l_mm }}</span>
                                <span class="badge bg-red">{{ $tl_mm }}</span>
                                <span class="badge bg-primary">{{ $k_mm }}</span> - Multimedia<br>
                                <span class="badge bg-green">{{ $l_fm }}</span>
                                <span class="badge bg-red">{{ $tl_fm }}</span>
                                <span class="badge bg-primary">{{ $k_fm }}</span> - Farmasi<br><br>

                                <span class="badge bg-green">Lulus</span>
                                <span class="badge bg-red">Tidak Lulus</span>
                                <span class="badge bg-primary">Kosong</span>
                            </div>
                        </div>
                    </div>
                <div class="col-sm-12 col-md-12">

                <div class="col-md-6">
                    <div class="box box-danger box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Siswa</h3>
                        </div>

                        <div class="box-body">
                            - Jumlah Siswa : {{$jmlsemua}} Orang
                            <hr>
                            - {{$jmllaki}} Laki - Laki<br>
                            - {{$jmlperempuan}} Perempuan
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-success box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Administrasi</h3>
                        </div>

                        <div class="box-body">
                            - Jumlah Siswa : {{$jmlsemua}} Orang
                            <hr>
                            - {{$jmllunas}} Lunas<br>
                            - {{$jmlblunas}} Belum Lunas
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">

                <div class="col-md-6">
                    <div class="box box-warning box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Daftar</h3>
                        </div>

                        <div class="box-body">
                            - Jumlah Siswa : {{$jmlsemua}} Orang
                            <hr>
                            - {{$jmlonline}} Daftar Online<br>
                            - {{$jmloffline}} Daftar Offline
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="box box-info box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Daftar Jurusan</h3>
                        </div>

                        <div class="box-body">
                            - Jumlah Siswa : {{$jmlsemua}} Orang
                            <hr>
                            {{$jmlap}} - Admistrasi Perkantoran<br>
                            {{$jmlak}} - Akuntansi<br>
                            {{$jmlpms}} - Pemasaran<br>
                            {{$jmltkj}} - Teknik Komputer dan Jaringan<br>
                            {{$jmlmtd}} - Multimedia<br>
                            {{$jmlfm}} - Farmasi
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection