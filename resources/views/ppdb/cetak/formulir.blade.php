@extends('layouts.ppdb')

@section('content')

    <section class="content-header">
        <h1>
            Cetak Formulir
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Siswa Baru</h3>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">
                        <a href="/ppdb/cetak/formulir-semua/print" target="_blank" class="btn btn-primary">Cetak Semua</a>
                        <a href="/ppdb/cetak/formulir-kosong/print" target="_blank" class="btn btn-warning">Cetak Formulir Kosong</a>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>ID Daftar</th>
                                    <th>JK</th>
                                    <th>Asal Sekolah</th>
                                    <th>Tgl Daftar</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Cetak</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswa as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->id_daftar }}</td>
                                        <td>{{ $item->jk }}</td>
                                        <td>{{ $item->asal_sekolah }}</td>
                                        <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                        <td>{{ $item->tahun_ajaran }}</td>
                                        <td><a href="/ppdb/cetak/formulir/print/{{$item->id}}" target="_blank" class="btn btn-success">Cetak</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
