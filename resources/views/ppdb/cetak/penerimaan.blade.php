<!DOCTYPE html>
<html>

<head>
    <title>Data Penerimaan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Source Sans Pro', sans-serif;
        }

        hr.new4 {
            border: 2px solid #000000;
        }

        td {
            padding: 2px 2px;
        }
    </style>
</head>

<body onload="window.print()" >
    <!-- onload="window.print()" -->
    <div class="container">
        <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <img src="{{asset('itlabil/image/default/logo.png')}}" width="150px">
                    </td>
                    <td align="center">
                        <b>
                            <h3>DATA PENERIMAAN SISWA/I BARU</h3>
                        </b>
                        <h5>
                            <font color="green">SEKOLAH MENENGAH KEJURUAN (SMK)</font>
                            <font color="red"> PELITA</font>
                        </h5>
                        <h4>BISNIS MANAJEMEN, TEKNIK INFORMATIKA DAN KESEHATAN</h4>
                        <h6>GEDONGTATAAN KABUPATEN PESAWARAN</h6>
                        <h6>TAHUN PELAJARAN 2020/2021</h6>
                    </td>
                    <td>
                        <img src="{{asset('itlabil/image/default/smk_bisa.png')}}" width="200px">
                    </td>
                </tr>
            </tbody>
        </table>
        <hr class="new4">
        <table width="100%" border="1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>JK</th>
                    <th>Asal Sekolah</th>
                    <th>Jurusan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($siswa as $item)
                <tr>
                    <td align="center">{{ $no++ }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->jk }}</td>
                    <td>{{ $item->asal_sekolah }}</td>
                    <td>{{ $item->jurusan_1 }}</td>
                    <td align="center">{{ $item->status_lulus }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <table width="100%">
            <tbody>
                <tr>
                    <td width="60%"></td>
                    <td width="40%" align="right">Gedongtataan, ...................................................</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center">Kepala Sekolah,</td>
                </tr>
                <tr height="80px">
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center"><u>(Aunurrofiq M., S.Sos., M.M.)</u></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>