<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nama</label>
                {!! Form::text('nama', null, ['class' => 'col-8 form-control','placeholder'=>'Nama Lengkap','title'=>'Nama Lengkap']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nama') }}</small>
            </div>

            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Jenis Kelamin</label>
                {!! Form::select('jk',array('laki-laki'=>'Laki - Laki','perempuan'=>'Perempuan') ,null,array('class'=>'col-8 form-control','title'=>'Jenis Kelamin')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('jk') }}</small>
            </div>

            <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Tempat Lahir</label>
                {!! Form::text('tempat_lahir', null, ['class' => 'col-8 form-control','placeholder'=>'Tempat Lahir','title'=>'Tempat Lahir']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
            </div>

            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Tanggal Lahir</label>
                {!! Form::date('tanggal_lahir', null, ['class' => 'col-8 form-control','title'=>'Tanggal Lahir']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
            </div>

            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Alamat</label>
                {!! Form::textarea('alamat', null, ['class' => 'col-8 form-control','placeholder'=>'Alamat','style' => 'width: 100%; height: 80px;']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('alamat') }}</small>
            </div>

            <div class="form-group{{ $errors->has('nisn') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">NISN</label>
                {!! Form::text('nisn', null, ['class' => 'col-8 form-control','placeholder'=>'NISN','title'=>'NISN']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nisn') }}</small>
            </div>

            <div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Agama</label>
                {!! Form::select('agama',$agama ,null,array('class'=>'col-8 form-control','title'=>'Agama')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('agama') }}</small>
            </div>

            <div class="form-group{{ $errors->has('kelas') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Kelas</label>
                {!! Form::select('kelas',array('10'=>'10','11'=>'11','12'=>'12') ,null,array('class'=>'col-8 form-control','title'=>'Kelas')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('kelas') }}</small>
            </div>

            <div class="form-group{{ $errors->has('asal_sekolah') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Asal Sekolah</label>
                {!! Form::text('asal_sekolah', null, ['class' => 'col-8 form-control','placeholder'=>'Asal Sekolah','title'=>'Asal Sekolah']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('asal_sekolah') }}</small>
            </div>

            <div class="form-group{{ $errors->has('nomor_sttb') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nomor STTB/Ijazah</label>
                {!! Form::text('nomor_sttb', null, ['class' => 'col-8 form-control','placeholder'=>'Nomor STTB/Ijazah','title'=>'Nomor STTB/Ijazah']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">Lewati Kolom jika belum ada STTB/Ijazah</small>
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nomor_sttb') }}</small>
            </div>

            <div class="form-group{{ $errors->has('nomor_skhu') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nomor SKHU</label>
                {!! Form::text('nomor_skhu', null, ['class' => 'col-8 form-control','placeholder'=>'Nomor SKHU','title'=>'Nomor SKHU']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">Lewati Kolom jika belum ada SKHU</small>
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nomor_skhu') }}</small>
            </div>

            <div class="form-group{{ $errors->has('nomor_peserta_ujian_smp') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nomor Peserta Ujian SMP</label>
                {!! Form::text('nomor_peserta_ujian_smp', null, ['class' => 'col-8 form-control','placeholder'=>'Nomor Peserta Ujian SMP','title'=>'Nomor Peserta Ujian SMP']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">Lewati Kolom jika tidak ada Nomor Peserta Ujian SMP</small>
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('nomor_peserta_ujian_smp') }}</small>
            </div>

            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nomor Telp/HP</label>
                {!! Form::text('telp', null, ['class' => 'col-8 form-control','placeholder'=>'Nomor Telp/HP','title'=>'Nomor Telp/HP']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('telp') }}</small>
            </div>

            <div class="form-group row">
                <label class="col-12 col-form-label">Pilihlah jurusan yang kamu minati : </label>
            </div>

            <div class="form-group{{ $errors->has('jurusan_1') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Jurusan 1</label>
                {!! Form::select('jurusan_1',$jurusan ,null,array('class'=>'col-8 form-control','title'=>'Jurusan 1')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('jurusan_1') }}</small>
            </div>

            <div class="form-group{{ $errors->has('jurusan_2') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Jurusan 2</label>
                {!! Form::select('jurusan_2',$jurusan ,null,array('class'=>'col-8 form-control','title'=>'Jurusan 2')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('jurusan_2') }}</small>
            </div>

        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="form-group row">
                <label class="col-12 col-form-label">Data Orang Tua : </label>
            </div>

            <div class="form-group{{ $errors->has('ayah_nama') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nama Ayah</label>
                {!! Form::text('ayah_nama', null, ['class' => 'col-8 form-control','placeholder'=>'Nama Ayah','title'=>'Nama Ayah']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ayah_nama') }}</small>
            </div>

            <div class="form-group{{ $errors->has('ayah_pendidikan') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Pendidikan Ayah</label>
                {!! Form::select('ayah_pendidikan',$status ,null,array('class'=>'col-8 form-control','title'=>'Pendidikan Ayah')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ayah_pendidikan') }}</small>
            </div>

            <div class="form-group{{ $errors->has('ayah_pekerjaan') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Pekerjaan Ayah</label>
                {!! Form::select('ayah_pekerjaan',$pekerjaan ,null,array('class'=>'col-8 form-control','title'=>'Pekerjaan Ayah')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ayah_pekerjaan') }}</small>
            </div>

            <div class="form-group{{ $errors->has('ibu_nama') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Nama Ibu</label>
                {!! Form::text('ibu_nama', null, ['class' => 'col-8 form-control','placeholder'=>'Nama Ibu','title'=>'Nama Ibu']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ibu_nama') }}</small>
            </div>

            <div class="form-group{{ $errors->has('ibu_pendidikan') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Pendidikan Ibu</label>
                {!! Form::select('ibu_pendidikan',$status ,null,array('class'=>'col-8 form-control','title'=>'Pendidikan Ibu')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ibu_pendidikan') }}</small>
            </div>

            <div class="form-group{{ $errors->has('ibu_pekerjaan') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Pekerjaan Ibu</label>
                {!! Form::select('ibu_pekerjaan',$pekerjaan ,null,array('class'=>'col-8 form-control','title'=>'Pekerjaan Ibu')) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('ibu_pekerjaan') }}</small>
            </div>

            <div class="form-group{{ $errors->has('alamat_orangtua') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Alamat Orang Tua</label>
                {!! Form::textarea('alamat_orangtua', null, ['class' => 'col-8 form-control','placeholder'=>'Alamat Orang Tua','style' => 'width: 100%; height: 80px;']) !!}
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('alamat_orangtua') }}</small>
            </div>

        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <div class="card-box">

            <div class="form-group row">
                <label class="col-12 col-form-label">Kelengkapan : </label>
            </div>

            <div class="form-group{{ $errors->has('img_ijazah') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Foto STTB/IJAZAH</label>
                {!! Form::file('img_ijazah') !!}
                <label class="col-4"></label>
                <label class="col-4 col-form-label"></label><small class="text-danger">Lewati Kolom jika belum ada STTB/Ijazah</small>
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('img_ijazah') }}</small>
            </div>

            <div class="form-group{{ $errors->has('img_skhu') ? ' has-error' : '' }} row">
                <label class="col-4 col-form-label">Foto SKHU</label>
                {!! Form::file('img_skhu') !!}
                <label class="col-4"></label>
                <label class="col-4 col-form-label"></label><small class="text-danger">Lewati Kolom jika belum ada SKHU</small>
                <label class="col-4 col-form-label"></label><small class="text-danger">{{ $errors->first('img_skhu') }}</small>
            </div>

        </div>
    </div>
</div>
<hr>
<div class="form-group" align="right">
    <div class="col-md-12">
        <button type="submit" class="btn btn-custom">
            Daftar
        </button>
    </div>
</div>