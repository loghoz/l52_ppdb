{{-- DATA SISWA  --}}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Data Siswa</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">

            {{-- NAMA  --}}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama', 'Nama Lengkap', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                </div>
            </div>

            {{-- JENIS KELAMIN  --}}
            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }}">
                {!! Form::label('jk', 'Jenis Kelamin', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-4">
                    <label class="radio-inline">
                        {!! Form::radio('jk', 'laki-laki') !!} Laki - Laki
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="radio-inline">
                        {!! Form::radio('jk', 'perempuan') !!} Perempuan
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                </div>
            </div>

            {{-- TEMPAT LAHIR  --}}
            <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tempat_lahir', 'Tempat Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('tempat_lahir', null, ['class' => 'form-control','placeholder'=>'Tempat Lahir']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                </div>
            </div>

            {{-- TANGGAL LAHIR  --}}
            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tanggal_lahir', 'Tanggal Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::date('tanggal_lahir', null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                </div>
            </div>

            {{-- ALAMAT  --}}
            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                {!! Form::label('alamat', 'Alamat', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('alamat', null, ['class' => 'form-control','placeholder'=>'Alamat']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('alamat') }}</small>
                </div>
            </div>

            {{-- NISN  --}}
            <div class="form-group{{ $errors->has('nisn') ? ' has-error' : '' }}">
                {!! Form::label('nisn', 'NISN', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nisn', null, ['class' => 'form-control','placeholder'=>'No Induk Siswa Nasional']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nisn') }}</small>
                </div>
            </div>

            {{-- AGAMA  --}}
            <div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }}">
                {!! Form::label('agama', 'Agama', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('agama',$agama ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('agama') }}</small>
                </div>
            </div>

            {{-- Kelas  --}}
            <div class="form-group{{ $errors->has('kelas') ? ' has-error' : '' }}">
                {!! Form::label('kelas', 'Kelas', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('kelas',array('10' => '10', '11' => '11', '12' => '12') ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('kelas') }}</small>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            {{-- Asal Sekolah  --}}
            <div class="form-group{{ $errors->has('asal_sekolah') ? ' has-error' : '' }}">
                {!! Form::label('asal_sekolah', 'Asal Sekolah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('asal_sekolah', null, ['class' => 'form-control','placeholder'=>'Asal Sekolah']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('asal_sekolah') }}</small>
                </div>
            </div>

            {{-- NOMOR STTB/IJAZAH  --}}
            <div class="form-group{{ $errors->has('nomor_sttb') ? ' has-error' : '' }}">
                {!! Form::label('nomor_sttb', 'Nomor STTB/Ijazah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nomor_sttb', null, ['class' => 'form-control','placeholder'=>'Nomor STTB/Ijazah']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nomor_sttb') }}</small>
                </div>
            </div>

            {{-- NOMOR SKHU  --}}
            <div class="form-group{{ $errors->has('nomor_skhu') ? ' has-error' : '' }}">
                {!! Form::label('nomor_skhu', 'Nomor SKHU', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nomor_skhu', null, ['class' => 'form-control','placeholder'=>'Nomor SKHU']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nomor_skhu') }}</small>
                </div>
            </div>

            {{-- NOMOR PESERTA  --}}
            <div class="form-group{{ $errors->has('nomor_peserta_ujian_smp') ? ' has-error' : '' }}">
                {!! Form::label('nomor_peserta_ujian_smp', 'Nomor Peserta Ujian SMP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nomor_peserta_ujian_smp', null, ['class' => 'form-control','placeholder'=>'Nomor Peserta Ujian SMP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nomor_peserta_ujian_smp') }}</small>
                </div>
            </div>

            {{-- TELP  --}}
            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                {!! Form::label('telp', 'Telp / HP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('telp', null, ['class' => 'form-control','placeholder'=>'Telp / HP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('telp') }}</small>
                </div>
            </div>

            <div class="form-group col-sm-12" align="center">
                <br><br>
                <p><strong>Pilihlah Jurusan yang anda minati.</strong></p>
            </div>

            {{-- Jurusan 1  --}}
            <div class="form-group{{ $errors->has('jurusan_1') ? ' has-error' : '' }}">
                {!! Form::label('jurusan_1', 'Jurusan 1', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('jurusan_1',$jurusan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jurusan_1') }}</small>
                </div>
            </div>

            {{-- Jurusan 2  --}}
            <div class="form-group{{ $errors->has('jurusan_2') ? ' has-error' : '' }}">
                {!! Form::label('jurusan_2', 'Jurusan 2', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('jurusan_2',$jurusan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jurusan_2') }}</small>
                </div>
            </div>

            {{-- Tahun Ajaran  --}}
            <div class="form-group{{ $errors->has('tahun_ajaran') ? ' has-error' : '' }}">
                {!! Form::label('tahun_ajaran', 'Tahun Ajaran', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('tahun_ajaran',$tadaf ,null,array('class'=>'form-control','disabled')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
                </div>
            </div>

            {{-- Tipe Daftar  --}}
            <div class="form-group{{ $errors->has('daftar') ? ' has-error' : '' }}">
                {!! Form::label('daftar', 'Tipe Daftar', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('daftar',array('Offline' => 'Offline','Online' => 'Online') ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('daftar') }}</small>
                </div>
            </div>

            {{-- Keterangan  --}}
            <div class="form-group{{ $errors->has('ket') ? ' has-error' : '' }}">
                {!! Form::label('ket', 'Keteragan', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('ket', null, ['class' => 'form-control','placeholder'=>'Keterangan']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ket') }}</small>
                </div>
            </div>
            {{-- Info  --}}
            <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                {!! Form::label('info', 'Info Siswa', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('info', null, ['class' => 'form-control','placeholder'=>'Info Siswa']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('info') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- ORTU WALI  --}}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Data Orang Tua</h3>
    </div>
    <div class="box-body">
        {{-- DATA AYAH  --}}
        <div class="col-md-12">

            <div class="form-group{{ $errors->has('ayah_nama') ? ' has-error' : '' }}">
                {!! Form::label('ayah_nama', 'Nama Ayah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('ayah_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ayah']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ayah_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pendidikan', 'Pendidikan Ayah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('ayah_pendidikan',$status ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ayah_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pekerjaan', 'Pekerjaan Ayah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('ayah_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ayah_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_nama') ? ' has-error' : '' }}">
                {!! Form::label('ibu_nama', 'Nama Ibu', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('ibu_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ibu']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ibu_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pendidikan', 'Pendidikan Ibu', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('ibu_pendidikan',$status ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ibu_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pekerjaan', 'Pekerjaan Ibu', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('ibu_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('ibu_pekerjaan') }}</small>
                </div>
            </div>

            {{-- ALAMAT  --}}
            <div class="form-group{{ $errors->has('alamat_orangtua') ? ' has-error' : '' }}">
                {!! Form::label('alamat_orangtua', 'Alamat Orangtua', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('alamat_orangtua', null, ['class' => 'form-control','placeholder'=>'Alamat Orangtua']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('alamat_orangtua') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Kelengkapan  --}}
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Kelengkapan</h3>
    </div>
    <div class="box-body">

        <div class="col-md-6">

            {{-- PHOTO STTB/IJAZAH  --}}
            <div class="form-group{{ $errors->has('img_ijazah') ? ' has-error' : '' }}">
                {!! Form::label('img_ijazah', 'Gambar STTB/Ijazah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('img_ijazah') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('img_ijazah') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {{-- PHOTO SKHU  --}}
            <div class="form-group{{ $errors->has('img_skhu') ? ' has-error' : '' }}">
                {!! Form::label('img_skhu', 'Gambar SKHU', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('img_skhu') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('img_skhu') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB</p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>