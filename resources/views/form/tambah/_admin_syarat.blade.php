<div class="col-sm-6">

  {{-- ket  --}}
  <div class="form-group{{ $errors->has('ket') ? ' has-error' : '' }}">
    {!! Form::label('ket', 'Keterangan', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::select('ket',array('pas-photo' => 'Pas Photo', 'skl' => 'Surat Keterangan Lulus', 'rapot' => 'Rapot', 'kk' => 'Kartu Keluarga') ,null,array('class'=>'form-control')) !!}
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('ket') }}</small>
    </div>
  </div>

  {{-- PHOTO  --}}
  <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
    {!! Form::label('photo', 'Gambar', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::file('photo') !!}
    </div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('photo') }}</small>
      <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB</p>
    </div>
  </div>
  {!! Form::hidden('siswa_id',$siswa->id ,null,array('class'=>'form-control')) !!}
  <div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
  </div>
</div>