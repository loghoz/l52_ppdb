<div class="col-sm-12">
    <div class="form-group{{ $errors->has('informasi') ? ' has-error' : '' }}">
        {!! Form::label('informasi', 'Informasi', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::textarea('informasi', null, ['class' => 'form-control','placeholder'=>'Informasi']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('informasi') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
        {!! Form::label('role', 'Role', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('role',array('Admin' => 'Admin', 'Siswa' => 'Siswa') ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('role') }}</small>
        </div>
    </div>

    {{-- PHOTO Informasi  --}}
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'Gambar', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::file('photo') !!}
        </div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('photo') }}</small>
            <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB</p>
        </div>
    </div>

    <div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
        {!! Form::label('publish', 'Publish', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('publish',array('Tidak Tampil' => 'Tidak Tampil', 'Tampil' => 'Tampil') ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('publish') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>