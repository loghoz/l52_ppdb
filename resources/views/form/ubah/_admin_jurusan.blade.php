<div class="col-sm-12">
  <div class="form-group{{ $errors->has('kode_jurusan') ? ' has-error' : '' }}">
    {!! Form::label('kode_jurusan', 'Kode Jurusan', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::text('kode_jurusan', null, ['class' => 'form-control','placeholder'=>'Kode Jurusan']) !!}
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('kode_jurusan') }}</small>
    </div>
  </div>
  <div class="form-group{{ $errors->has('jurusan') ? ' has-error' : '' }}">
    {!! Form::label('jurusan', 'Jurusan', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::text('jurusan', null, ['class' => 'form-control','placeholder'=>'Jurusan']) !!}
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('jurusan') }}</small>
    </div>
  </div>

  <div class="form-group{{ $errors->has('bidang_keahlian') ? ' has-error' : '' }}">
    {!! Form::label('bidang_keahlian', 'Bidang Keahlian', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::text('bidang_keahlian', null, ['class' => 'form-control','placeholder'=>'Bidang Keahlian']) !!}
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('bidang_keahlian') }}</small>
    </div>
  </div>

  <div class="form-group{{ $errors->has('daftar_ulang') ? ' has-error' : '' }}">
    {!! Form::label('daftar_ulang', 'Daftar Ulang', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      <div class="box-body pad">
        {!! Form::textarea('daftar_ulang', null, ['class' => 'textarea','placeholder'=>'Daftar Ulang','style' => 'width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
      </div>
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('daftar_ulang') }}</small>
    </div>
  </div>

  <div class="form-group{{ $errors->has('info_jurusan') ? ' has-error' : '' }}">
    {!! Form::label('info_jurusan', 'Info Jurusan', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      <div class="box-body pad">
        {!! Form::textarea('info_jurusan', null, ['class' => 'textarea','placeholder'=>'Info Jurusan','style' => 'width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
      </div>
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('info_jurusan') }}</small>
    </div>
  </div>

  <div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
    {!! Form::label('publish', 'Publish', ['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
      {!! Form::select('publish',array('F' => 'Tidak Tampil', 'T' => 'Tampil') ,null,array('class'=>'form-control')) !!}
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('publish') }}</small>
    </div>
  </div>

  <div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
  </div>
</div>