@extends('layouts.kajur')

@section('content')

<section class="content-header">
    <h1>
        Beranda
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-6">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Siswa</h3>
                    </div>
                    <div class="box-body">
                        - Jumlah Siswa : {{$jmlsemua}} Orang
                        <hr>
                        - {{$jmllaki}} Laki - Laki<br>
                        - {{$jmlperempuan}} Perempuan
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Penerimaan</h3>
                    </div>
                    <div class="box-body">
                        - Jumlah Siswa : {{$jmlsemua}} Orang
                        <hr>
                        - {{$jmllulus}} Lulus<br>
                        - {{$jmltlulus}} Tidak Lulus<br>
                        - {{$jmlkosong}} Masih Kosong
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection