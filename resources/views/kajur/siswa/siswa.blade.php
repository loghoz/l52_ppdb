@extends('layouts.kajur')

@section('content')

    <section class="content-header">
        <h1>
            Data Calon Siswa
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body" style="overflow-x:auto;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>JK</th>
                                    <th>Telp</th>
                                    <th>Asal Sekolah</th>
                                    <th>Jurusan 1</th>
                                    <th>Jurusan 2</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Syarat</th>
                                    <th>Ubah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswa as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->jk }}</td>
                                        <td>{{ $item->telp }}</td>
                                        <td>{{ $item->asal_sekolah }}</td>
                                        <td>{{ $item->jurusan_1 }}</td>
                                        <td>{{ $item->jurusan_2 }}</td>
                                        <td>{{ $item->ket }}</td>
                                        @if($item->status_lulus=="-")
                                            <td><span class="badge bg-primary">Status Kosong</span></td>
                                        @elseif($item->status_lulus=="Lulus")
                                            <td><span class="badge bg-green">{{ $item->status_lulus }}</span></td>
                                        @else
                                            <td><span class="badge bg-red">{{ $item->status_lulus }}</span></td>
                                        @endif
                                        <td>
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default{{$item->id}}">
                                            Lihat
                                        </button>
                                        <div class="modal fade in" id="modal-default{{$item->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title">Data Persyaratan - {{$item->nama}}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    @foreach($item->syarat as $syarat)
                                                    @if($syarat->ket=="pas-photo")
                                                    Pas Photo<br><br>
                                                    <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
                                                    <br><br>
                                                    @endif
                                                    @endforeach
                                                    <hr>
                                                    @foreach($item->syarat as $syarat)
                                                    @if($syarat->ket=="skl")
                                                    Surat Keterangan Lulus<br><br>
                                                    <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
                                                    <br><br>
                                                    @endif
                                                    @endforeach
                                                    <hr>

                                                    @foreach($item->syarat as $syarat)
                                                    @if($syarat->ket=="rapot")
                                                    Rapot<br><br>
                                                    <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
                                                    <br><br>
                                                    @endif
                                                    @endforeach
                                                    <hr>

                                                    @foreach($item->syarat as $syarat)
                                                    @if($syarat->ket=="kk")
                                                    Kartu Keluarga<br><br>
                                                    <img src="{{url('itlabil/image/persyaratan')}}/{{$syarat->photo}}" alt="{{$syarat->photo}}" class="img-thumbnail" width="700px"><br><br>
                                                    <br><br>
                                                    @endif
                                                    @endforeach
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </td>
                                        <td>
                                        {!! Form::model($item->id, ['route' => ['kajur.siswa.update', $item->id],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                                            {!! Form::submit('Ubah', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
