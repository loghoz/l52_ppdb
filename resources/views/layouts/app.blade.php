<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>SMK Pelita Pesawaran</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Website Pendaftaran Peserta Didik Baru SMK Pelita Pesawaran" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('itlabil/image/default/logo.png') }}">

        <!-- App css -->
        <link href="{{ asset('itlabil/user/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('itlabil/user/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('itlabil/user/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ asset('itlabil/user/assets/js/modernizr.min.js') }}"></script>
        
        <link href="{{ asset('itlabil/user/toast/toastr.min.css') }}" rel="stylesheet">

        <!-- DataTables -->
        <link href="{{ asset('itlabil/user/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('itlabil/user/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ asset('itlabil/user/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

    </head>

    <body>
        
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <div class="logo">
                        <a href="/" class="logo">
                            <img src="{{ asset('itlabil/image/default/logo.png') }}" alt="" height="26" class="logo-small">
                            <img src="{{ asset('itlabil/image/default/logo_pelita.png') }}" alt="" height="40" class="logo-large">
                        </a>

                    </div>

                    <div class="menu-extras topbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="menu-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>
                            <li class="notification-list hide-phone">
                                <a class="nav-link waves-effect nav-user" href="mailto:smkpelitasmk@yahoo.com" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-email"></i> smkpelitasmk@yahoo.com 
                                </a>
                            </li>
                            <li class="notification-list hide-phone">
                                <a class="nav-link waves-effect nav-user" href="tel:072194092" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-phone"></i> 072194092 
                                </a>
                            </li>
                            <!-- BAHASA -->
                            <!-- <li class="dropdown notification-list hide-phone">
                                <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-earth"></i> English  <i class="mdi mdi-chevron-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">

                                     
                                    <a href="javascript:void(0);" class="dropdown-item">
                                        Spanish
                                    </a>

                                     
                                    <a href="javascript:void(0);" class="dropdown-item">
                                        Italian
                                    </a>

                                     
                                    <a href="javascript:void(0);" class="dropdown-item">
                                        French
                                    </a>

                                     
                                    <a href="javascript:void(0);" class="dropdown-item">
                                        Russian
                                    </a>

                                </div>
                            </li> -->
                        </ul>
                    </div>

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="/"><i class="icon-home"></i>Beranda</a>
                            </li>

                            <li class="has-submenu">
                                <a href="/daftar"><i class="icon-user"></i>Daftar</a>
                            </li>

                            <!-- <li class="has-submenu">
                                <a href="/bayardaftar"><i class="icon-basket"></i>Bayar</a>
                            </li> -->

                            <li class="has-submenu">
                                <a href="/cekstatus"><i class="icon-info"></i>Cek Status</a>
                            </li>

                            <!-- <li class="has-submenu">
                                <a href="/alumni"><i class="icon-user"></i>Alumni</a>
                            </li> -->
                            <!-- <li class="has-submenu">
                                <a href="#"><i class="icon-user"></i>Alumni</a>
                                <ul class="submenu">
                                    <li><a href="/alumni">Data Alumni</a></li>
                                    <li><a href="/tambah_alumni">Isi Biodata Alumni</a></li>
                                </ul>
                            </li> -->
                            <li class="has-submenu">
                                <a href="/kontak"><i class="icon-phone"></i>Kontak</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            @yield('content')
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2019 © iTLab <a href="http://smkpelitapesawaran.sch.id">SMK Pelita Pesawaran</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="itlabil/user/assets/js/jquery.min.js"></script>
        <script src="itlabil/user/assets/js/popper.min.js"></script>
        <script src="itlabil/user/assets/js/bootstrap.min.js"></script>
        <script src="itlabil/user/assets/js/waves.js"></script>
        <script src="itlabil/user/assets/js/jquery.slimscroll.js"></script>

        <!-- Flot chart -->
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.min.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="itlabil/user/plugins/flot-chart/curvedLines.js"></script>
        <script src="itlabil/user/plugins/flot-chart/jquery.flot.axislabels.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="itlabil/user/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="itlabil/user/plugins/jquery-knob/jquery.knob.js"></script>

        <!-- Dashboard Init -->
        <script src="itlabil/user/assets/pages/jquery.dashboard.init.js"></script>

        <!-- App js -->
        <script src="itlabil/user/assets/js/jquery.core.js"></script>
        <script src="itlabil/user/assets/js/jquery.app.js"></script>

        <script type="text/javascript" src="{{ asset('itlabil/user/toast/toastr.min.js') }}"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','info')}}"

                switch(type){
                    case 'info':
                        toastr.info("{{ Session::get('message') }}");
                        break;
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'warning':
                        toastr.warning("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>

        <!-- Required datatable js -->
        <script src="{{ asset('itlabil/user/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Buttons examples -->
        <script src="{{ asset('itlabil/user/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/buttons.print.min.js') }}"></script>

        <!-- Key Tables -->
        <script src="{{ asset('itlabil/user/plugins/datatables/dataTables.keyTable.min.js') }}"></script>

        <!-- Responsive examples -->
        <script src="{{ asset('itlabil/user/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('itlabil/user/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>
</html>