<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusLulus extends Model
{
    protected $table = 'status_luluses';

    protected $fillable = [
        'siswa_id','status_lulus',
    ];

    // relasi ke siswa
    public function siswa()
    {
        return $this->belongsTo('App\Berita', 'siswa_id');
    }
}
