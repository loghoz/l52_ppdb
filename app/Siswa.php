<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $fillable = [
        'id_daftar', 'nama', 'jk', 'tempat_lahir', 'tanggal_lahir', 'nisn', 'alamat', 'kelas', 'agama', 'asal_sekolah', 'nomor_sttb', 'nomor_skhu', 'nomor_peserta_ujian_smp', 'ayah_nama', 'ayah_pekerjaan', 'ayah_pendidikan', 'ibu_nama', 'ibu_pendidikan', 'ibu_pekerjaan', 'alamat_orangtua', 'telp', 'img_ijazah', 'img_skhu', 'jurusan_1', 'jurusan_2', 'status_bayar', 'status_lulus', 'tahun_ajaran', 'daftar', 'ket','info'
    ];
    // relasi ke berita
    public function syarat()
    {
        return $this->hasMany('App\Persyaratan', 'siswa_id');
    }
}
