<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusans';

    protected $fillable = [
        'kode_jurusan','jurusan','bidang_keahlian','daftar_ulang','info_jurusan','publish'
    ];
}
