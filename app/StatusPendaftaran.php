<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusPendaftaran extends Model
{
    protected $table = 'status_pendaftarans';

    protected $fillable = [
        'status_pendaftaran',
    ];
}
