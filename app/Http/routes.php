<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('/home', 'HomeController@index');

Route::resource('/', 'Web\User\BerandaController');
Route::resource('/daftar', 'Web\User\DaftarController');
Route::resource('/cekdaftar', 'Web\User\CekDaftarController');
Route::resource('/kartudaftar', 'Web\User\KartuDaftarController');
Route::resource('/cekstatus', 'Web\User\CekStatusController@create');
Route::resource('/persyaratan', 'Web\User\PersyaratanController');
Route::get('/persyaratan/{id}/{ket}', 'Web\User\PersyaratanController@show');
Route::resource('/status', 'Web\User\CekStatusController');
Route::resource('/bayardaftar', 'Web\User\BayarDaftarController');
Route::resource('/kontak', 'Web\User\KontakController');
Route::get('/pdfkartudaftar/{id}', 'Web\User\DaftarController@kartudaftar');
Route::get('/pdfbayar/{id}', 'Web\User\DaftarController@bayar');
Route::get('/downloadkartu/{id}', 'Web\User\DaftarController@downloadkartu');

Route::auth();

//ADMIN
Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/siswa/create', 'Web\Admin\SiswaController@create');
Route::resource('/admin/siswa', 'Web\Admin\SiswaController');
Route::put('/admin/persyaratan/update/{id}/{tipe}', 'Web\Admin\PersyaratanController@update');
Route::resource('/admin/infolulus', 'Web\Admin\LulusController');
Route::put('/admin/infolulus/update/{id}', 'Web\Admin\LulusController@update');
Route::resource('/admin/pendaftaran', 'Web\Admin\PendaftaranController');
Route::put('/admin/infolulus/update/{id}', 'Web\Admin\LulusController@update');
Route::resource('/admin/history', 'Web\Admin\HistoryController');
Route::resource('/admin/kontak', 'Web\Admin\KontakController');
Route::resource('/admin/informasi', 'Web\Admin\InformasiController');
Route::resource('/admin/persyaratan', 'Web\Admin\PersyaratanController');
Route::resource('/admin/cetak/formulir', 'Web\Admin\CetakController');
Route::get('/admin/cetak/formulir/print/{id}', 'Web\Admin\CetakController@formulir');

Route::get('/admin/cetak/datasiswa', 'Web\Admin\CetakController@datasiswa');
Route::get('/admin/cetak/datasiswaexcel/{type}', 'Web\Admin\CetakController@datasiswaexcel');

Route::get('/admin/cetak/formulir-semua/print', 'Web\Admin\CetakController@formulirsemua');
Route::get('/admin/cetak/formulir-kosong/print', 'Web\Admin\CetakController@formulirkosong');
Route::resource('/admin/cetak/penerimaan', 'Web\Admin\CetakController@penerimaan');
Route::resource('/admin/cetak/pendaftaran', 'Web\Admin\CetakController@pendaftaran');
Route::resource('/admin/pengaturan', 'Web\Admin\PengaturanController');
Route::resource('/admin/jurusan', 'Web\Admin\JurusanController');

//KAJUR

Route::resource('/kajur/beranda', 'Web\Kajur\BerandaController');
Route::resource('/kajur/siswa', 'Web\Kajur\SiswaController');

//PPDB

Route::resource('/ppdb/beranda', 'Web\AdminPPDB\BerandaController');
Route::resource('/ppdb/siswa', 'Web\AdminPPDB\SiswaController');
Route::resource('/ppdb/persyaratan', 'Web\AdminPPDB\PersyaratanController');
Route::resource('/ppdb/cetak/formulir', 'Web\AdminPPDB\CetakController');
Route::get('/ppdb/cetak/formulir/print/{id}', 'Web\AdminPPDB\CetakController@formulir');
Route::get('/ppdb/cetak/formulir-semua/print', 'Web\AdminPPDB\CetakController@formulirsemua');
Route::get('/ppdb/cetak/formulir-kosong/print', 'Web\AdminPPDB\CetakController@formulirkosong');

Route::resource('/ppdb/cetak/penerimaan', 'Web\AdminPPDB\CetakController@penerimaan');