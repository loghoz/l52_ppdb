<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiswaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'nama'                      => 'required',
                    'tempat_lahir'              => 'required',
                    'tanggal_lahir'             => 'required',
                    'nisn'                      => 'required|unique:siswas',
                    'alamat'                    => 'required',
                    'asal_sekolah'              => 'required',
                    // 'nomor_sttb'                => 'required',
                    // 'nomor_skhu'                => 'required',
                    // 'nomor_peserta_ujian_smp'   => 'required',
                    // 'ayah_nama'                 => 'required',
                    // 'ayah_pekerjaan'            => 'required',
                    // 'ayah_pendidikan'           => 'required',
                    // 'ibu_nama'                  => 'required',
                    // 'ibu_pekerjaan'             => 'required',
                    // 'ibu_pendidikan'            => 'required',
                    'alamat_orangtua'           => 'required',
                    'telp'                      => 'required'
                ];
            }

            case 'PUT':
            case 'PATCH':
            {
                return [
                    'nama'                      => 'required',
                    'tempat_lahir'              => 'required',
                    'tanggal_lahir'             => 'required',
                    'nisn'                      => 'required',
                    'alamat'                    => 'required',
                    'asal_sekolah'              => 'required',
                    // 'nomor_sttb'                => 'required',
                    // 'nomor_skhu'                => 'required',
                    // 'nomor_peserta_ujian_smp'   => 'required',
                    // 'ayah_nama'                 => 'required',
                    // 'ayah_pekerjaan'            => 'required',
                    // 'ayah_pendidikan'           => 'required',
                    // 'ibu_nama'                  => 'required',
                    // 'ibu_pekerjaan'             => 'required',
                    // 'ibu_pendidikan'            => 'required',
                    'alamat_orangtua'           => 'required',
                    'telp'                      => 'required'
                ];
            }

            default:break;
        }
    }

    public function messages()
    {
        return [

            'nama.required'                      => 'Tidak boleh kosong',
            'tempat_lahir.required'              => 'Tidak boleh kosong',
            'tanggal_lahir.required'             => 'Tidak boleh kosong',
            'nisn.required'                      => 'Tidak boleh kosong',
            'nisn.unique'                      	 => 'NISN anda sudah terdaftar di server.',
            'alamat.required'                    => 'Tidak boleh kosong',
            'asal_sekolah.required'              => 'Tidak boleh kosong',
            // 'nomor_sttb.required'                => 'Tidak boleh kosong',
            // 'nomor_skhu.required'                => 'Tidak boleh kosong',
            // 'nomor_peserta_ujian_smp.required'   => 'Tidak boleh kosong',
            // 'ayah_nama.required'                 => 'Tidak boleh kosong',
            // 'ayah_pekerjaan.required'            => 'Tidak boleh kosong',
            // 'ayah_pendidikan.required'           => 'Tidak boleh kosong',
            // 'ibu_nama.required'                  => 'Tidak boleh kosong',
            // 'ibu_pekerjaan.required'             => 'Tidak boleh kosong',
            // 'ibu_pendidikan.required'            => 'Tidak boleh kosong',
            'alamat_orangtua.required'           => 'Tidak boleh kosong',
            'telp.required'                      => 'Tidak boleh kosong',
        ];
    }
}
