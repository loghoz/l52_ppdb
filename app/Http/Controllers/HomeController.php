<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == "admin") { 
            return redirect()->route('ppdb.beranda.index');
        }elseif(Auth::user()->role == "kajur"){
            return redirect()->route('kajur.beranda.index');
        } elseif(Auth::user()->role == "superadmin"){
            return redirect()->route('admin.beranda.index');
        } else {
            return redirect('/');
        }
    }
}
