<?php

namespace App\Http\Controllers\Web\Kajur;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Meta;
use App\Siswa;
use Auth;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:kajur');
    }

    public function index()
    {
        $tahun_aktif = Meta::where('id', 1)->get()->first();

        $name = Auth::user()->name;

        if ($name=='Kajur TKJ') {
            $jurusan="Teknik Komputer dan Jaringan";
        } else if($name=='Kajur MM'){
            $jurusan="Multimedia";
        } else if($name=='Kajur FM'){
            $jurusan="Farmasi";
        } else if($name=='Kajur AP'){
            $jurusan="Administrasi Perkantoran";
        } else if($name=='Kajur AK'){
            $jurusan="Akuntansi";
        } else {
            $jurusan="Pemasaran";
        }

        $jmlsemua = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', $jurusan)->count();
        $jmllaki = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jk', 'laki-laki')->where('jurusan_1', $jurusan)->count();
        $jmlperempuan = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jk', 'perempuan')->where('jurusan_1', $jurusan)->count();

        $jmllulus = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('status_lulus', 'Lulus')->where('jurusan_1', $jurusan)->count();
        $jmltlulus = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('status_lulus', 'Tidak Lulus')->where('jurusan_1', $jurusan)->count();
        $jmlkosong = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('status_lulus', '-')->where('jurusan_1', $jurusan)->count();

        return view('kajur.beranda', compact('jmlsemua','jmllaki','jmlperempuan','jmllulus','jmltlulus','jmlkosong'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
