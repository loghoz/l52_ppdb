<?php

namespace App\Http\Controllers\Web\Kajur;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Meta;
use App\Siswa;
use App\History;
use Auth;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:kajur');
    }

    public function index()
    {
        $no = 1;

        $tahun_aktif = Meta::where('id',1)->get()->first();

        $name = Auth::user()->name;

        if ($name=='Kajur TKJ') {
            $jurusan="Teknik Komputer dan Jaringan";
        } else if($name=='Kajur MM'){
            $jurusan="Multimedia";
        } else if($name=='Kajur FM'){
            $jurusan="Farmasi";
        } else if($name=='Kajur AP'){
            $jurusan="Administrasi Perkantoran";
        } else if($name=='Kajur AK'){
            $jurusan="Akuntansi";
        } else {
            $jurusan="Pemasaran";
        }
        
        $siswa = Siswa::where('tahun_ajaran',$tahun_aktif->meta_value)
                        ->where('jurusan_1',$jurusan)
                        ->orderBy('nama','ASC')->get()->all();

        return view('kajur.siswa.siswa', compact('siswa','no'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);

        if ($siswa->status_lulus=="Lulus") {
            if($siswa->jurusan_1==$siswa->jurusan_2){
                $datasiswa['status_lulus'] = "Tidak Lulus";
                $datasiswa['jurusan_2'] = '-';
                $datasiswa['ket'] = $siswa->ket.' - Tidak Lulus di Jurusan '.$siswa->jurusan_1;
            }else{
                if ($siswa->jurusan_2=='-') {
                    $datasiswa['status_lulus'] = "Tidak Lulus";
                } else {
                    $datasiswa['status_lulus'] = "-";
                    $datasiswa['jurusan_1'] = $siswa->jurusan_2;
                    $datasiswa['jurusan_2'] = '-';
                    $datasiswa['ket'] = $siswa->ket.' - Tidak Lulus di Jurusan '.$siswa->jurusan_1;
                }
            }
        } else {
            $datasiswa['status_lulus'] = "Lulus";
        }
        

        $siswa->update($datasiswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Status Penerimaan atas nama ".$siswa->nama." menjadi ".$datasiswa['status_lulus'];
        History::create($histori);

        $notification = array(
            'message' => 'Status Penerimaan Siswa "'.$siswa['nama'].'" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('kajur.siswa.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
