<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\Persyaratan;
use App\History;

use Auth;

class PersyaratanController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $siswa = Siswa::where('id', $request->siswa_id)->get()->first();

        // SIMPAN DATA SISWA
        $data = $request->all();

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'), $request->ket, $siswa->nama);
        } else {
            $data['photo'] = '-';
        }

        Persyaratan::create($data);

        // HISTORI
        $histori['user'] = $siswa->nama;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Upload Persyaratan " . $data['ket'];
        History::create($histori);

        $notification = array(
            'message' => 'Upload "' . $data['ket'] . '" berhasil.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id, $ket)
    {
        $data = Persyaratan::where('siswa_id', $id)->where('ket', $ket)->get()->all();

        return view('user.persyaratan.show', compact('data'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $data = Persyaratan::findOrFail($id);
        $siswa = Siswa::findOrFail($data->siswa_id);

        $this->deletePhoto($data->photo);

        // HISTORI
        $histori['user'] = $siswa->nama;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Menghapus Data Persyaratan " . $data->ket;
        History::create($histori);

        $notification = array(
            'message' => 'Data "' . $data->ket . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        Persyaratan::find($id)->delete();

        return redirect()->back()->with($notification);
    }
    protected function savePhoto(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/persyaratan';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/persyaratan/' . $filename;
        return File::delete($path);
    }
}
