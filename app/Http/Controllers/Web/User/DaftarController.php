<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;
use App\Http\Requests\SiswaRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\Jurusan;
use App\Status;
use App\Pekerjaan;
use App\Agama;
use App\History;
use App\Meta;
use Auth;

use App\StatusPendaftaran;

use PDF;

class DaftarController extends Controller
{

    public function index()
    {
        $jurusan = Jurusan::where('jurusan', 'not like', "-")->where('publish', "T")->lists('jurusan','jurusan');
        $agama = Agama::lists('agama', 'agama');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $status = Status::lists('status', 'status');
        $stt_daftar = StatusPendaftaran::where('id', 1)->get()->all();
        return view('user.daftar.daftar', compact('jurusan', 'status', 'pekerjaan', 'agama', 'stt_daftar'));
    }

    public function create()
    {
        //
    }
    public function kartudaftar(Request $request, $id)
    {
        $data = Siswa::where('id', $id)->get()->first();
        return view('user.download.view-kartu', compact('data'));
    }
    public function bayar(Request $request, $id)
    {
        $data = Siswa::where('id', $id)->get()->first();
        return view('user.download.view-pembayaran', compact('data'));
    }
    public function downloadkartu(Request $request, $id)
    {
        $data = Siswa::where('id', $id)->get()->first();

        $pdf = PDF::loadview('user.download.download-kartu', compact('data'));
        return $pdf->download('kartu-daftar.pdf');
    }

    public function store(SiswaRequest $request)
    {
        $tahun = Meta::where('id', 2)->get()->first();

        // SIMPAN DATA SISWA
        $siswa = $request->all();
        $siswa['status_bayar'] = 'Belum Lunas';
        $siswa['status_lulus'] = '-';
        $siswa['daftar'] = 'Online';
        $siswa['tahun_ajaran'] = $tahun->meta_value;
        $siswa['id_daftar'] = str_random(10);

        if ($request->hasFile('img_ijazah')) {
            $siswa['img_ijazah'] = $this->saveIjazah($request->file('img_ijazah'), 'ijazah', $request->nama);
        } else {
            $siswa['img_ijazah'] = '-';
        }

        if ($request->hasFile('img_skhu')) {
            $siswa['img_skhu'] = $this->saveSKHU($request->file('img_skhu'), 'skhu', $request->nama);
        } else {
            $siswa['img_skhu'] = '-';
        }

        Siswa::create($siswa);

        // HISTORI
        $histori['user'] = $siswa['nama'];
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Mendaftar sebagai siswa baru ";
        History::create($histori);

        $notification = array(
            'message' => 'Selamat Pendaftaran dengan nama "' . $siswa['nama'] . '" berhasil.',
            'alert-type' => 'info'
        );

        $dnama = $siswa['nama'];
        $dnisn = $siswa['nisn'];

        $datadaftar = Siswa::where('nama', $dnama)->where('nisn', $dnisn)->first();

        return redirect()->route('kartudaftar.index', ['id_daftar' => $datadaftar->id_daftar])->with($notification);
    }

    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('user.daftar.show_data', compact('siswa'));
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);

        $jurusan = Jurusan::lists('jurusan', 'jurusan');
        $agama = Agama::lists('agama', 'agama');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $status = Status::lists('status', 'status');

        return view('user.daftar.edit_data', compact('siswa', 'jurusan', 'status', 'pekerjaan', 'agama'));
    }

    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);

        $datasiswa = $request->only(
            'nama',
            'jk',
            'tempat_lahir',
            'tanggal_lahir',
            'nisn',
            'alamat',
            'kelas',
            'agama',
            'asal_sekolah',
            'nomor_sttb',
            'nomor_skhu',
            'nomor_peserta_ujian_smp',
            'ayah_nama',
            'ayah_pendidikan',
            'ayah_pekerjaan',
            'ibu_nama',
            'ibu_pendidikan',
            'ibu_pekerjaan',
            'alamat_orangtua',
            'telp',
            'jurusan_1',
            'jurusan_2'
        );

        if ($request->hasFile('img_ijazah')) {
            $this->deletePhoto($siswa->img_ijazah);
            $datasiswa['img_ijazah'] = $this->saveIjazah($request->file('img_ijazah'), 'ijazah', $request->nama);
        }

        if ($request->hasFile('img_skhu')) {
            $this->deletePhoto($siswa->img_skhu);
            $datasiswa['img_skhu'] = $this->saveSKHU($request->file('img_skhu'), 'skhu', $request->nama);
        }

        $siswa->update($datasiswa);

        // HISTORI
        $histori['user'] = $datasiswa['nama'];
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Mengubah Data Diri";
        History::create($histori);

        $notification = array(
            'message' => 'Data Diri berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('status.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }

    protected function saveIjazah(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/image_user';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    protected function saveSKHU(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/image_user';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/image_user/' . $filename;
        return File::delete($path);
    }
}
