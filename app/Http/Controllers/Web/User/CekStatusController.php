<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\StatusPendaftaran;
use App\Informasi;
use App\Persyaratan;

class CekStatusController extends Controller
{

    public function index(Request $request)
    {
        $id_daftar = $request->cookie('id_daftar');
        $siswa = Siswa::where('id_daftar', $id_daftar)->first();
        $stt_pen = StatusPendaftaran::where('id', 2)->first();
        $info = Informasi::where('role', 'Siswa')->orderBy('id', 'DESC')->get()->all();
        $spas = Persyaratan::where('siswa_id', $siswa->id)->where('ket', 'pas-photo')->count();
        $skk = Persyaratan::where('siswa_id', $siswa->id)->where('ket', 'kk')->count();
        $srapot = Persyaratan::where('siswa_id', $siswa->id)->where('ket', 'rapot')->count();
        $sskl = Persyaratan::where('siswa_id', $siswa->id)->where('ket', 'skl')->count();

        return view('user.cek.status', compact('siswa', 'stt_pen', 'info', 'spas', 'skk', 'srapot', 'sskl'));
    }

    public function create()
    {
        return view('user.cek.beranda');
    }

    public function store(Request $request)
    {
        $id_daftar = $request->get('id_daftar');
        $siswa = Siswa::where('id_daftar', 'LIKE', $id_daftar)->first();

        if (!empty($siswa)) {
            $id_daftar = cookie()->forever('id_daftar', $siswa->id_daftar);

            return redirect()->route('status.index')->withCookie($id_daftar);
        } else {
            $notification = array(
                'message' => 'ID DAFTAR Salah, Mohon di Cek Kembali.',
                'alert-type' => 'warning'
            );

            return redirect('cekstatus')->with($notification);
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
