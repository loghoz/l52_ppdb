<?php

namespace App\Http\Controllers\Web\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Siswa;
use App\Jurusan;

class CekDaftarController extends Controller
{

    public function index()
    {
        return view('user.daftar.list');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $nama = $request->get('nama');
        $siswa = Siswa::where('nama', 'LIKE', $nama)->first();

        if (!empty($siswa)) {
            return redirect()->route('cekdaftar.show', ['id' => $siswa->nama]);
        } else {
            $notification = array(
                'message' => 'Nama yang kamu masukkan tidak TERDAFTAR, Mohon di Cek Kembali.',
                'alert-type' => 'warning'
            );

            return redirect('cekdaftar')->with($notification);
        }
    }

    public function show($id)
    {
        $jurusan = Jurusan::all();
        $siswa = Siswa::where('nama', $id)->get()->all();
        return view('user.daftar.showlist',compact('siswa','jurusan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
