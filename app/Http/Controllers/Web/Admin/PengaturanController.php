<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Meta;
use App\Tahun;
use App\Status;
use App\Pekerjaan;
use App\Agama;
use App\Jurusan;
use App\History;
use Auth;

class PengaturanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;

        $dataTa = Tahun::lists('tahun', 'tahun');

        $tahun = Meta::where('id',1)->get()->first();
        $tadaf = Meta::where('id',2)->get()->first();
        

        return view('admin.pengaturan.beranda',compact('no','tahun','dataTa','tadaf'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        if ($id==1) {
            $data = Meta::findOrFail($id);

            $data->update($request->all());

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Ubah";
            $histori['desc_info'] = "Tahun Ajaran yang Aktif ".$data['meta_value'];
            History::create($histori);

            $notification = array(
                'message' => 'Tahun Ajaran yang Aktif '.$data['meta_value'],
                'alert-type' => 'success'
            );

            return redirect()->route('admin.pengaturan.index')->with($notification);
        } else if($id==2) {
            $data = Meta::findOrFail($id);

            $data->update($request->all());

            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Ubah";
            $histori['desc_info'] = "Tahun Ajaran Daftar yang Aktif ".$data['meta_value'];
            History::create($histori);

            $notification = array(
                'message' => 'Tahun Ajaran Daftar yang Aktif '.$data['meta_value'],
                'alert-type' => 'success'
            );

            return redirect()->route('admin.pengaturan.index')->with($notification);
        } else {
            return redirect()->route('admin.pengaturan.index');
        }
    }

    public function destroy($id)
    {
        //
    }
}
