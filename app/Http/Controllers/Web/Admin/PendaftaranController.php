<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\StatusPendaftaran;
use App\History;
use Auth;

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }
    
    public function index()
    {
        $no = 1;

        $data = StatusPendaftaran::where('id',1)->get()->all();

        return view('admin.status.pendaftaran', compact('data','no'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = StatusPendaftaran::findOrFail($id);

        if ($data->status_pendaftaran=="Aktif") {
            $datadaftar['status_pendaftaran'] = "Tidak Aktif";
        } else {
            $datadaftar['status_pendaftaran'] = "Aktif";
        }
        

        $data->update($datadaftar);
        
        if ($id==1) {
            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Ubah";
            $histori['desc_info'] = "Status Pendaftaran menjadi ".$datadaftar['status_pendaftaran'];
            History::create($histori);

            $notification = array(
                'message' => 'Status Pendaftaran berhasil diubah.',
                'alert-type' => 'success'
            );

            return redirect()->route('admin.pendaftaran.index')->with($notification);
        } else {
            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Ubah";
            $histori['desc_info'] = "Status Penerimaan menjadi ".$datadaftar['status_pendaftaran'];
            History::create($histori);

            $notification = array(
                'message' => 'Status Penerimaan berhasil diubah.',
                'alert-type' => 'success'
            );

            return redirect()->route('admin.infolulus.index')->with($notification);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
