<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\Meta;
use App\Informasi;
use DB;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $info = Informasi::where('role', 'Admin')->orderBy('created_at', 'DESC')->get()->all();
        $jmlsemua = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->count();
        $jmllaki = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jk', 'laki-laki')->count();
        $jmlperempuan = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jk', 'perempuan')->count();

        $jmllunas = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('status_bayar', 'Lunas')->count();
        $jmlblunas = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('status_bayar', 'Belum Lunas')->count();

        $jmlonline = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('daftar', 'Online')->count();
        $jmloffline = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('daftar', 'Offline')->count();

        $jmlap = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Administrasi Perkantoran')->count();
        $jmlak = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Akuntansi')->count();
        $jmltkj = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Teknik Komputer dan Jaringan')->count();
        $jmlpms = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Pemasaran')->count();
        $jmlfm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Farmasi')->count();
        $jmlmtd = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Multimedia')->count();

        $l_ap = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Administrasi Perkantoran')->where('status_lulus', 'Lulus')->count();
        $tl_ap = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Administrasi Perkantoran')->where('status_lulus', 'Tidak Lulus')->count();
        $k_ap = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Administrasi Perkantoran')->where('status_lulus', '-')->count();

        $l_ak = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Akuntansi')->where('status_lulus', 'Lulus')->count();
        $tl_ak = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Akuntansi')->where('status_lulus', 'Tidak Lulus')->count();
        $k_ak = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Akuntansi')->where('status_lulus', '-')->count();

        $l_pms = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Pemasaran')->where('status_lulus', 'Lulus')->count();
        $tl_pms = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Pemasaran')->where('status_lulus', 'Tidak Lulus')->count();
        $k_pms = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Pemasaran')->where('status_lulus', '-')->count();

        $l_tkj = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Teknik Komputer dan Jaringan')->where('status_lulus', 'Lulus')->count();
        $tl_tkj = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Teknik Komputer dan Jaringan')->where('status_lulus', 'Tidak Lulus')->count();
        $k_tkj = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Teknik Komputer dan Jaringan')->where('status_lulus', '-')->count();

        $l_mm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Multimedia')->where('status_lulus', 'Lulus')->count();
        $tl_mm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Multimedia')->where('status_lulus', 'Tidak Lulus')->count();
        $k_mm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Multimedia')->where('status_lulus', '-')->count();

        $l_fm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Farmasi')->where('status_lulus', 'Lulus')->count();
        $tl_fm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Farmasi')->where('status_lulus', 'Tidak Lulus')->count();
        $k_fm = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->where('jurusan_1', 'Farmasi')->where('status_lulus', '-')->count();        
        
        return view('admin.beranda', compact('info', 'jmlsemua', 'jmllaki', 'jmlperempuan', 'jmllunas', 'jmlblunas', 'jmlonline', 'jmloffline', 'jmlmtd', 'jmlfm', 'jmltkj', 'jmlpms', 'jmlak', 'jmlap',
                    'l_ap','tl_ap','k_ap','l_ak','tl_ak','k_ak','l_pms','tl_pms','k_pms','l_tkj','tl_tkj','k_tkj','l_mm','tl_mm','k_mm','l_fm','tl_fm','k_fm'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
