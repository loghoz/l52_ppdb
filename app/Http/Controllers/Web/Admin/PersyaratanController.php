<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\Meta;
use App\Persyaratan;
use App\History;

use Auth;

class PersyaratanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('admin.persyaratan.syarat', compact('siswa', 'no'));
    }

    public function create()
    {
    }

    public function store(Request $request)
    {

        $siswa = Siswa::findOrFail($request->siswa_id);
        // SIMPAN DATA SISWA
        $data = $request->all();

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'), $request->ket, $siswa->nama);
        } else {
            $data['photo'] = '-';
        }

        Persyaratan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Upload Persyaratan " . $data['ket'];
        History::create($histori);

        $notification = array(
            'message' => 'Upload "' . $data['ket'] . '" berhasil.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        $data = Persyaratan::where('siswa_id', $id)->get()->all();
        $siswa = Siswa::findOrFail($id);

        return view('admin.persyaratan.show', compact('data', 'siswa'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $datasiswa = $request->only('tipe');
        
        if ($datasiswa['tipe'] == "Ijazah") {
            $this->deletePhotoSiswa($siswa->img_ijazah);
            $datasiswa['img_ijazah'] = "-";
        }

        if ($datasiswa['tipe'] == "SKHU") {
            $this->deletePhotoSiswa($siswa->img_skhu);
            $datasiswa['img_skhu'] = "-";
        }

        $siswa->update($datasiswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Delete";
        $histori['desc_info'] = "Gambar " . $datasiswa['tipe'] . " Siswa atas nama " . $siswa->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Gambar ' . $datasiswa['tipe'] . ' Siswa "' . $siswa->nama . '" berhasil dihapus.',
            'alert-type' => 'danger'
        );

        return redirect()->back()->with($notification);
    }

    public function destroy($id)
    {
        $data = Persyaratan::findOrFail($id);
        $siswa = Siswa::findOrFail($data->siswa_id);

        $this->deletePhoto($data->photo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Menghapus Data Persyaratan " . $data->ket . " atas nama " . $siswa->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data ' . $data->ket . ' Atas nama ' . $siswa->nama . ' berhasil dihapus.',
            'alert-type' => 'error'
        );

        Persyaratan::find($id)->delete();

        return redirect()->back()->with($notification);
    }
    protected function savePhoto(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/persyaratan';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/persyaratan/' . $filename;
        return File::delete($path);
    }

    public function deletePhotoSiswa($filename)
    {
        $path = 'itlabil/image/image_user/' . $filename;
        return File::delete($path);
    }
}
