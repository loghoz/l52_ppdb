<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;
use App\History;
use App\Meta;
use Auth;
use Excel;

class CetakController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;

        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('admin.cetak.formulir', compact('siswa', 'no'));
    }

    public function formulir(Request $request, $id)
    {
        $data = Siswa::where('id', $id)->get()->first();

        return view('admin.cetak.print_formulir', compact('data'));
    }

    public function datasiswa(Request $request)
    {
        $no = 1;
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('admin.cetak.view_data_siswa', compact('siswa', 'tahun_aktif', 'no'));
    }

    public function datasiswaexcel($type)
    {
        $tahun_aktif = Meta::where('id', 2)->get()->first();
        $data = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return Excel::create('itsolutionstuff_example', function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function formulirsemua()
    {
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $tahun_daftar = Meta::where('id', 2)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('admin.cetak.print_formulir_semua', compact('siswa', 'tahun_daftar'));
    }

    public function formulirkosong()
    {
        return view('admin.cetak.print_formulir_kosong');
    }

    public function pendaftaran()
    {
        $no = 1;
        $siswa = Siswa::all();
        return view('admin.cetak.pendaftaran', compact('siswa', 'no'));
    }
    public function penerimaan()
    {
        $no = 1;

        $siswa = Siswa::orderBy('jurusan_1', 'ASC')->orderBy('nama', 'ASC')->orderBy('status_lulus', 'ASC')->get()->all();
        return view('admin.cetak.penerimaan', compact('siswa', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
