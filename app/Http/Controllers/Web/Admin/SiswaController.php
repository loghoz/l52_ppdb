<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SiswaRequest;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\Jurusan;
use App\Status;
use App\Pekerjaan;
use App\Agama;
use App\History;
use App\Meta;
use App\Tahun;
use Auth;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('admin.siswa.siswa', compact('siswa', 'no'));
    }

    public function create()
    {
        $jurusan = Jurusan::where('jurusan', 'not like', "-")->lists('jurusan','jurusan');
        $agama = Agama::lists('agama', 'agama');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $status = Status::lists('status', 'status');

        $tadaf = Meta::where('id', 2)->lists('meta_value', 'meta_value');

        return view('admin.siswa.tambah_siswa', compact('jurusan', 'status', 'pekerjaan', 'agama', 'tadaf'));
    }

    public function store(SiswaRequest $request)
    {
        $tahun = Meta::where('id', 2)->get()->first();
        // SIMPAN DATA SISWA
        $siswa = $request->all();
        $siswa['status_bayar'] = 'Belum Lunas';
        $siswa['status_lulus'] = '-';
        $siswa['id_daftar'] = str_random(10);
        $siswa['tahun_ajaran'] = $tahun->meta_value;

        if ($request->hasFile('img_ijazah')) {
            $siswa['img_ijazah'] = $this->saveIjazah($request->file('img_ijazah'), 'ijazah', $request->nama);
        } else {
            $siswa['img_ijazah'] = '-';
        }

        if ($request->hasFile('img_skhu')) {
            $siswa['img_skhu'] = $this->saveSKHU($request->file('img_skhu'), 'skhu', $request->nama);
        } else {
            $siswa['img_skhu'] = '-';
        }

        Siswa::create($siswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Siswa Baru atas nama " . $siswa['nama'];
        History::create($histori);

        $notification = array(
            'message' => 'Siswa Baru "' . $siswa['nama'] . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.siswa.create')->with($notification);
    }

    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('admin.siswa.show_siswa', compact('siswa'));
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);

        $jurusan = Jurusan::lists('jurusan', 'jurusan');
        $agama = Agama::lists('agama', 'agama');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $status = Status::lists('status', 'status');
        $tadaf = Tahun::lists('tahun', 'tahun');

        return view('admin.siswa.edit_siswa', compact('siswa', 'jurusan', 'status', 'pekerjaan', 'agama', 'tadaf'));
    }

    public function update(SiswaRequest $request, $id)
    {
        $siswa = Siswa::findOrFail($id);

        $datasiswa = $request->only(
            'nama',
            'jk',
            'tempat_lahir',
            'tanggal_lahir',
            'nisn',
            'alamat',
            'kelas',
            'agama',
            'asal_sekolah',
            'nomor_sttb',
            'nomor_skhu',
            'nomor_peserta_ujian_smp',
            'ayah_nama',
            'ayah_pendidikan',
            'ayah_pekerjaan',
            'ibu_nama',
            'ibu_pendidikan',
            'ibu_pekerjaan',
            'alamat_orangtua',
            'telp',
            'jurusan_1',
            'jurusan_2',
            'status_bayar',
            'tahun_ajaran',
            'daftar',
            'ket','info'
        );

        if ($request->hasFile('img_ijazah')) {
            $this->deletePhoto($siswa->img_ijazah);
            $datasiswa['img_ijazah'] = $this->saveIjazah($request->file('img_ijazah'), 'ijazah', $request->nama);
        }

        if ($request->hasFile('img_skhu')) {
            $this->deletePhoto($siswa->img_skhu);
            $datasiswa['img_skhu'] = $this->saveSKHU($request->file('img_skhu'), 'skhu', $request->nama);
        }

        $siswa->update($datasiswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Siswa atas nama " . $datasiswa['nama'];
        History::create($histori);

        $notification = array(
            'message' => 'Data Siswa "' . $datasiswa['nama'] . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.siswa.index')->with($notification);
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);

        $this->deletePhoto($siswa->img_ijazah);
        $this->deletePhoto($siswa->img_skhu);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Siswa atas nama " . $siswa->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Siswa "' . $siswa->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        Siswa::find($id)->delete();
        return redirect()->route('admin.siswa.index')->with($notification);
    }

    protected function saveIjazah(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/image_user';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    protected function saveSKHU(UploadedFile $photo, $ket, $nama)
    {
        $fileName = $nama . '-' . $ket . '-' . str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/image_user';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/image_user/' . $filename;
        return File::delete($path);
    }
}
