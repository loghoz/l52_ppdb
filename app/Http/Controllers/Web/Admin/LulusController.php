<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Siswa;
use App\History;
use App\Meta;
use Auth;

use App\StatusPendaftaran;

class LulusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }
    
    public function index()
    {
        $no = 1;

        $tahun_aktif = Meta::where('id',1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran',$tahun_aktif->meta_value)->orderBy('nama','ASC')->get()->all();

        $data = StatusPendaftaran::where('id',2)->get()->all();

        return view('admin.siswa.lulus', compact('siswa','no','data'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);

        if ($siswa->status_lulus=="Lulus") {
            if($siswa->jurusan_1==$siswa->jurusan_2){
                $datasiswa['status_lulus'] = "Tidak Lulus";
                $datasiswa['ket'] = $siswa->ket.' (Tidak Lulus di Jurusan '.$siswa->jurusan_1.')';
            }else{
                $datasiswa['status_lulus'] = "-";
                $datasiswa['jurusan_1'] = $siswa->jurusan_2;
            }
        } else {
            $datasiswa['status_lulus'] = "Lulus";
        }
        

        $siswa->update($datasiswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Status Penerimaan atas nama ".$siswa->nama." menjadi ".$datasiswa['status_lulus'];
        History::create($histori);

        $notification = array(
            'message' => 'Status Penerimaan Siswa "'.$siswa['nama'].'" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.infolulus.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
