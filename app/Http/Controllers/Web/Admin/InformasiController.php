<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Informasi;
use App\History;
use Auth;

class InformasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;
        $data = Informasi::all();

        return view('admin.info.info', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // SIMPAN DATA SISWA
        $data = $request->all();

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            $data['photo'] = '-';
        }

        Informasi::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Informasi baru telah di tambahkan";
        History::create($histori);

        $notification = array(
            'message' => 'Informasi baru berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.informasi.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Informasi::findOrFail($id);

        return view('admin.info.edit_info', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Informasi::findOrFail($id);

        $datainfo = $request->only('informasi', 'publish', 'role');

        if ($request->hasFile('photo')) {
            $this->deletePhoto($data->photo);
            $datainfo['photo'] = $this->savePhoto($request->file('photo'));
        }

        $data->update($datainfo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Informasi dengan id " . $data->id . " Telah diperbarui";
        History::create($histori);

        $notification = array(
            'message' => 'Informasi dengan id "' . $data->id . '" Telah diperbarui.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.informasi.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Informasi::findOrFail($id);
        $this->deletePhoto($data->photo);
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Informasi dengan id " . $data->id . " Telah dihapus";
        History::create($histori);

        $notification = array(
            'message' => 'Informasi dengan id "' . $data->id . '" Telah dihapus.',
            'alert-type' => 'error'
        );

        Informasi::find($id)->delete();

        return redirect()->route('admin.informasi.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/info';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/info/' . $filename;
        return File::delete($path);
    }
}
