<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kontak;
use App\History;
use Auth;

class KontakController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }
    
    public function index()
    {
        $kontak = Kontak::get()->all();
        return view('admin.kontak.kontak',compact('kontak'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $kontak = $request->all();
        Kontak::create($kontak);
        
        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Kontak Baru dengan nama Daftar ".$kontak['meta_key'];
        History::create($histori);

        $notification = array(
            'message' => 'Data Kontak berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.kontak.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $kontak = Kontak::findOrFail($id);

        return view('admin.kontak.edit_kontak', compact('kontak'));
    }

    public function update(Request $request, $id)
    {
        $kontak = Kontak::findOrFail($id);

        $kontak->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Kontak dengan nama Daftar ".$kontak['meta_key'];
        History::create($histori);

        $notification = array(
            'message' => 'Data Kontak berhasil diubah.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.kontak.index')->with($notification);
    }

    public function destroy($id)
    {
        $kontak = Kontak::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Kontah dengan nama Daftar".$kontak->meta_key;
        History::create($histori);

        $notification = array(
            'message' => 'Data Kontak berhasil dihapus.',
            'alert-type' => 'error'
        );
        Kontak::find($id)->delete();
        return redirect()->route('admin.kontak.index')->with($notification);
    }
}
