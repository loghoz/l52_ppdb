<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jurusan;
use Illuminate\Support\Facades\Auth;

class JurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superadmin');
    }

    public function index()
    {
        $no = 1;
        $data = Jurusan::where('kode_jurusan','NOT LIKE','-')->get()->all();

        return view('admin.jurusan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Jurusan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Jurusan baru ".$request->jurusan."telah di tambahkan";
        History::create($histori);

        $notification = array(
            'message' => 'Jurusan baru '.$request->jurusan.' berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.jurusan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Jurusan::findOrFail($id);

        return view('admin.jurusan.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Jurusan::findOrFail($id);

        $datainfo = $request->all();

        $data->update($datainfo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Jurusan " . $data->jurusan . " Telah diperbarui";
        History::create($histori);

        $notification = array(
            'message' => 'Jurusan "' . $data->jurusan . '" Telah diperbarui.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.jurusan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Jurusan::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Jurusan " . $data->jurusan . " Telah dihapus";
        History::create($histori);

        $notification = array(
            'message' => 'Informasi dengan id "' . $data->jurusan . '" Telah dihapus.',
            'alert-type' => 'error'
        );

        Jurusan::find($id)->delete();

        return redirect()->route('admin.jurusan.index')->with($notification);
    }

}
