<?php

namespace App\Http\Controllers\Web\AdminPPDB;

use App\Agama;
use App\History;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SiswaRequest;
use App\Jurusan;
use App\Meta;
use App\Pekerjaan;
use App\Siswa;
use App\Status;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('ppdb.siswa.siswa', compact('siswa', 'no'));
    }

    public function create()
    {
        $jurusan = Jurusan::where('jurusan', 'not like', "-")->lists('jurusan','jurusan');
        $agama = Agama::lists('agama', 'agama');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $status = Status::lists('status', 'status');

        $tadaf = Meta::where('id', 2)->lists('meta_value', 'meta_value');

        return view('ppdb.siswa.tambah_siswa', compact('jurusan', 'status', 'pekerjaan', 'agama', 'tadaf'));
    }

    public function store(SiswaRequest $request)
    {
        $tahun = Meta::where('id', 2)->get()->first();
        // SIMPAN DATA SISWA
        $siswa = $request->all();
        $siswa['status_bayar'] = 'Belum Lunas';
        $siswa['status_lulus'] = '-';
        $siswa['id_daftar'] = str_random(10);
        $siswa['tahun_ajaran'] = $tahun->meta_value;

        if ($request->hasFile('img_ijazah')) {
            $siswa['img_ijazah'] = $this->saveIjazah($request->file('img_ijazah'), 'ijazah', $request->nama);
        } else {
            $siswa['img_ijazah'] = '-';
        }

        if ($request->hasFile('img_skhu')) {
            $siswa['img_skhu'] = $this->saveSKHU($request->file('img_skhu'), 'skhu', $request->nama);
        } else {
            $siswa['img_skhu'] = '-';
        }

        Siswa::create($siswa);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Siswa Baru atas nama " . $siswa['nama'];
        History::create($histori);

        $notification = array(
            'message' => 'Siswa Baru "' . $siswa['nama'] . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('ppdb.siswa.create')->with($notification);
    }


    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('ppdb.siswa.show_siswa', compact('siswa'));
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
