<?php

namespace App\Http\Controllers\Web\AdminPPDB;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Meta;
use App\Siswa;

class CetakController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;

        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('ppdb.cetak.formulir', compact('siswa', 'no'));
    }

    public function formulir(Request $request, $id)
    {
        $data = Siswa::where('id', $id)->get()->first();

        return view('ppdb.cetak.print_formulir', compact('data'));
    }

    public function datasiswa(Request $request)
    {
        $no = 1;
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('ppdb.cetak.view_data_siswa', compact('siswa', 'tahun_aktif', 'no'));
    }

    public function datasiswaexcel($type)
    {
        $tahun_aktif = Meta::where('id', 2)->get()->first();
        $data = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();
    }

    public function formulirsemua()
    {
        $tahun_aktif = Meta::where('id', 1)->get()->first();
        $tahun_daftar = Meta::where('id', 2)->get()->first();
        $siswa = Siswa::where('tahun_ajaran', $tahun_aktif->meta_value)->orderBy('id', 'DESC')->get()->all();

        return view('ppdb.cetak.print_formulir_semua', compact('siswa', 'tahun_daftar'));
    }

    public function formulirkosong()
    {
        return view('ppdb.cetak.print_formulir_kosong');
    }

    public function pendaftaran()
    {
        $no = 1;
        $siswa = Siswa::all();
        return view('ppdb.cetak.pendaftaran', compact('siswa', 'no'));
    }
    public function penerimaan()
    {
        $no = 1;

        $siswa = Siswa::orderBy('jurusan_1', 'ASC')->orderBy('nama', 'ASC')->orderBy('status_lulus', 'ASC')->get()->all();
        return view('ppdb.cetak.penerimaan', compact('siswa', 'no'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
