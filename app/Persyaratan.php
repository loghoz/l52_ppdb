<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persyaratan extends Model
{
    protected $table = 'persyaratans';

    protected $fillable = [
        'siswa_id', 'ket', 'photo',
    ];

    // relasi ke berita
    public function siswa()
    {
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }
}
