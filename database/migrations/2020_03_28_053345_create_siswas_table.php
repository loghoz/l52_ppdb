<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_daftar');
            $table->string('nama');
            $table->string('jk');
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('nisn');
            $table->text('alamat');
            $table->string('kelas');
            $table->string('agama');
            $table->text('asal_sekolah');
            $table->string('nomor_sttb');
            $table->string('nomor_skhu');
            $table->string('nomor_peserta_ujian_smp');
            $table->string('ayah_nama');
            $table->string('ayah_pendidikan');
            $table->string('ayah_pekerjaan');
            $table->string('ibu_nama');
            $table->string('ibu_pendidikan');
            $table->string('ibu_pekerjaan');
            $table->text('alamat_orangtua');
            $table->string('telp');
            $table->string('img_ijazah');
            $table->string('img_skhu');
            $table->string('jurusan_1');
            $table->string('jurusan_2');
            $table->string('status_bayar');
            $table->string('status_lulus');
            $table->string('tahun_ajaran');
            $table->string('daftar');
            $table->string('ket');
            $table->string('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswas');
    }
}
