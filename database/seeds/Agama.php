<?php

use Illuminate\Database\Seeder;

class Agama extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Agama::create([
            'agama'=>'Islam'
        ]);
        App\Agama::create([
            'agama'=>'Kristen'
        ]);
        App\Agama::create([
            'agama'=>'Katholik'
        ]);
        App\Agama::create([
            'agama'=>'Hindu'
        ]);
        App\Agama::create([
            'agama'=>'Budha'
        ]);
        App\Agama::create([
            'agama'=>'Lainnya'
        ]);
    }
}
