<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Status::create([
            'status' => '-'
        ]);
        App\Status::create([
            'status' => 'Tidak Sekolah'
        ]);
        App\Status::create([
            'status' => 'Putus SD'
        ]);
        App\Status::create([
            'status' => 'SD Sederajat'
        ]);
        App\Status::create([
            'status' => 'SMP Sederajat'
        ]);
        App\Status::create([
            'status' => 'SMA Sederajat'
        ]);
        App\Status::create([
            'status' => 'D1'
        ]);
        App\Status::create([
            'status' => 'D2'
        ]);
        App\Status::create([
            'status' => 'D3'
        ]);
        App\Status::create([
            'status' => 'S1'
        ]);
        App\Status::create([
            'status' => 'S2'
        ]);
        App\Status::create([
            'status' => 'S3'
        ]);
    }
}
