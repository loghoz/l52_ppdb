<?php

use Illuminate\Database\Seeder;

class Kontak extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Kontak::create([
            'meta_key'      => 'Alamat',
            'meta_value'    => 'Jl. Raya Penengahan No. 04 Gedongtataan',
        ]);
        App\Kontak::create([
            'meta_key'      => 'No Telp. Sekolah',
            'meta_value'    => '0721 94092',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Email',
            'meta_value'    => 'smkpelitasmk@yahoo.com',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Facebook',
            'meta_value'    => 'https://www.facebook.com/smkpelita.pesawaran',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Instagram',
            'meta_value'    => '-',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Group Whatsapp',
            'meta_value'    => '-',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Group Telegram',
            'meta_value'    => '-',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Admin PPDB 1',
            'meta_value'    => 'Admin 1 - 08961111111',
        ]);
        App\Kontak::create([
            'meta_key'      => 'Admin PPDB 2',
            'meta_value'    => 'Admin 2 - 08961111111',
        ]);
    }
}
