<?php

use Illuminate\Database\Seeder;

class Tahun extends Seeder
{

    public function run()
    {
        App\Tahun::create(['tahun' => '2019 / 2020']);
        App\Tahun::create(['tahun' => '2020 / 2021']);
        App\Tahun::create(['tahun' => '2021 / 2022']);
        App\Tahun::create(['tahun' => '2022 / 2023']);
        App\Tahun::create(['tahun' => '2023 / 2024']);
        App\Tahun::create(['tahun' => '2024 / 2025']);
    }
}
