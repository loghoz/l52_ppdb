<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Agama::class);
        $this->call(Jurusan::class);
        $this->call(Kontak::class);
        $this->call(Pekerjaan::class);
        $this->call(Status::class);
        $this->call(StatusPendaftaran::class);
        $this->call(Tahun::class);
        $this->call(User::class);
    }
}
