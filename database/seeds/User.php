<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'email' => 'admin@smkpelitapesawaran.sch.id',
            'name' => 'Administrator',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'adminppdb1@smkpelitapesawaran.sch.id',
            'name' => 'Admin PPDB 1',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'adminppdb2@smkpelitapesawaran.sch.id',
            'name' => 'Admin PPDB 2',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'kepsek@smkpelitapesawaran.sch.id',
            'name' => 'Kepala Sekolah',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'waka@smkpelitapesawaran.sch.id',
            'name' => 'Wakil Kepala Sekolah',
            'password' => bcrypt('1sampai9')
        ]);
    }
}
