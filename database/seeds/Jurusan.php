<?php

use Illuminate\Database\Seeder;

class Jurusan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        App\Jurusan::create([
            'kode_jurusan' => 'AP',
            'jurusan' => 'Administrasi Perkantoran',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        App\Jurusan::create([
            'kode_jurusan' => 'AK',
            'jurusan' => 'Akuntansi',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        App\Jurusan::create([
            'kode_jurusan' => 'PMS',
            'jurusan' => 'Pemasaran',
            'bidang_keahlian'=>'Bisnis Manajemen'
        ]);
    
        App\Jurusan::create([
            'kode_jurusan' => 'TKJ',
            'jurusan' => 'Teknik Komputer dan Jaringan',
            'bidang_keahlian'=>'Teknik Komputer dan Informatika'
        ]);
    
        App\Jurusan::create([
            'kode_jurusan' => 'MM',
            'jurusan' => 'Multimedia',
            'bidang_keahlian'=>'Teknik Komputer dan Informatika'
        ]);
    
        App\Jurusan::create([
            'kode_jurusan' => 'FM',
            'jurusan' => 'Farmasi',
            'bidang_keahlian'=>'Kesehatan'
        ]);
    }
}
